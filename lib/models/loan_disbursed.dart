import 'package:fc_inputs/models/farmer.dart';

class LoanDisbursed {
  int id;
  int farmerId;
  var amount;
  int statusId;
  int userId;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  Farmer farmer;
  List<String> loanBundles;

  LoanDisbursed(
      {this.id,
      this.farmerId,
      this.amount,
      this.statusId,
      this.userId,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.farmer,
      this.loanBundles});

  LoanDisbursed.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    farmerId = json['farmer_id'];
    amount = json['amount'];
    statusId = json['status_id'];
    userId = json['user_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    farmer =
        json['farmer'] != null ? new Farmer.fromJson(json['farmer']) : null;
    loanBundles = json['loan_bundles'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['farmer_id'] = this.farmerId;
    data['amount'] = this.amount;
    data['status_id'] = this.statusId;
    data['user_id'] = this.userId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.farmer != null) {
      data['farmer'] = this.farmer.toJson();
    }
    data['loan_bundles'] = this.loanBundles;
    return data;
  }
}
