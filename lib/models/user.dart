import 'dart:convert';

class User {
  int id;
  String firstName;
  String lastName;
  String avatar;
  String phoneNumber;
  String emailVerifiedAt;
  String isPhoneVerified;
  String isDefaultPassChanged;
  String farmStateId;
  String farmStateName;
  String roleId;
  String deletedAt;
  String createdAt;
  String updatedAt;
  String password;
  String passwordConfirmation;


  User(
      {this.id,
      this.firstName,
      this.lastName,
      this.avatar,
      this.phoneNumber,
      this.emailVerifiedAt,
      this.isPhoneVerified,
      this.isDefaultPassChanged,
      this.farmStateId,
      this.roleId,
      this.deletedAt,
      this.createdAt,
      this.updatedAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    avatar = json['avatar'];
    phoneNumber = json['phone_number'];
    emailVerifiedAt = json['email_verified_at'];
    isPhoneVerified = json['is_phone_verified'];
    isDefaultPassChanged = json['is_default_pass_changed'];
    farmStateId = json['farm_state_id'].toString();
    roleId = json['role_id'].toString();
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    // data['avatar'] = this.avatar;
    data['phone_number'] = this.phoneNumber;
    data['farm_state_id'] = this.farmStateId;
    data['role_id'] = this.roleId;
    data['password'] = this.password;
    data['password_confirmation'] = this.passwordConfirmation;
    data['request_type'] = "registerWithPhone";
    return data;
  }
  
  Map<String, dynamic> toJson2() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['avatar'] = this.avatar;
    data['phone_number'] = this.phoneNumber;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['is_phone_verified'] = this.isPhoneVerified;
    data['is_default_pass_changed'] = this.isDefaultPassChanged;
    data['farm_state_id'] = this.farmStateId;
    data['role_id'] = this.roleId;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['password'] = this.password;
    data['password_confirmation'] = this.passwordConfirmation;
    return data;
  }

  
}

List<User> userFromJson(String jsonData) {
  final data = json.decode(jsonData)["data"];
  // print('res: ' + data.toString());
  return List<User>.from(data.map((item) => User.fromJson(item)));
}

String userToJson(User data) {
  final jsonData = data.toJson();
  print('jsonData: ' + jsonData.toString());
  return json.encode(jsonData);
}

