import 'dart:convert';

class Farmer {
  int id;
  int partner;
  int partner2;
  String crop;
  String gender;
  String firstName;
  String middleName;
  String lastName;
  String phone;
  String bVN;
  int state;
  String numberOfLivestock;
  String farmerBVN;
  String lGA;
  String farmerPhone;
  String farmSizeAcres;
  String stateLGA;
  String phoneLength;
  int pin;
  String deletedAt;
  String createdAt;
  String updatedAt;

  Farmer(
      {this.id,
      this.partner,
      this.partner2,
      this.crop,
      this.gender,
      this.firstName,
      this.middleName,
      this.lastName,
      this.phone,
      this.bVN,
      this.state,
      this.numberOfLivestock,
      this.farmerBVN,
      this.lGA,
      this.farmerPhone,
      this.farmSizeAcres,
      this.stateLGA,
      this.phoneLength,
      this.pin,
      this.deletedAt,
      this.createdAt,
      this.updatedAt});

  Farmer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    partner = json['Partner'];
    partner2 = json['Partner2'];
    crop = json['Crop'];
    gender = json['Gender'];
    firstName = json['First_Name'];
    middleName = json['Middle_Name'];
    lastName = json['Last_Name'];
    phone = json['Phone'];
    bVN = json['BVN'];
    state = json['State'];
    numberOfLivestock = json['NumberOfLivestock'];
    farmerBVN = json['FarmerBVN'];
    lGA = json['LGA'];
    farmerPhone = json['FarmerPhone'];
    farmSizeAcres = json['Farm_Size_Acres'];
    stateLGA = json['STATE_LGA'];
    phoneLength = json['phoneLength'];
    pin = json['pin'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['Partner'] = this.partner;
    data['Partner2'] = this.partner2;
    data['Crop'] = this.crop;
    data['Gender'] = this.gender;
    data['First_Name'] = this.firstName;
    data['Middle_Name'] = this.middleName;
    data['Last_Name'] = this.lastName;
    data['Phone'] = this.phone;
    data['BVN'] = this.bVN;
    data['State'] = this.state;
    data['NumberOfLivestock'] = this.numberOfLivestock;
    data['FarmerBVN'] = this.farmerBVN;
    data['LGA'] = this.lGA;
    data['FarmerPhone'] = this.farmerPhone;
    data['Farm_Size_Acres'] = this.farmSizeAcres;
    data['STATE_LGA'] = this.stateLGA;
    data['phoneLength'] = this.phoneLength;
    data['pin'] = this.pin;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

List<Farmer> farmerFromJson(String jsonData) {
  final data = json.decode(jsonData)["data"];
  // print('res: ' + data.toString());
  return List<Farmer>.from(data.map((item) => Farmer.fromJson(item)));
}

String farmerToJson(Farmer data) {
  final jsonData = data.toJson();
  print('jsonData: ' + jsonData.toString());
  return json.encode(jsonData);
}
