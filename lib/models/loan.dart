import 'package:fc_inputs/models/load_bundle.dart';

class Loan {
  int id;
  int farmerId;
  var amount;
  int statusId;
  int userId;
  String createdAt;
  String updatedAt;
  String deletedAt;
  List<LoanBundle> loanBundles;

  Loan(
      {this.id,
      this.farmerId,
      this.amount,
      this.statusId,
      this.userId,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.loanBundles});

  Loan.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    farmerId = json['farmer_id'];
    amount = json['amount'];
    statusId = json['status_id'];
    userId = json['user_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    if (json['loan_bundles'] != null) {
      loanBundles = new List<LoanBundle>();
      json['loan_bundles'].forEach((v) {
        loanBundles.add(new LoanBundle.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['farmer_id'] = this.farmerId;
    data['amount'] = this.amount;
    data['status_id'] = this.statusId;
    data['user_id'] = this.userId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.loanBundles != null) {
      data['loan_bundles'] = this.loanBundles.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

