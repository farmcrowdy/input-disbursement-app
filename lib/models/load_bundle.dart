class LoanBundle {
  int id;
  String name;
  String description;
  int statusId;
  String deletedAt;
  String createdAt;
  String updatedAt;
  var price;

  LoanBundle(
      {this.id,
      this.name,
      this.description,
      this.statusId,
      this.deletedAt,
      this.createdAt,
      this.updatedAt,
      this.price});

  LoanBundle.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    statusId = json['status_id'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['status_id'] = this.statusId;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['price'] = this.price;
    return data;
  }
}
