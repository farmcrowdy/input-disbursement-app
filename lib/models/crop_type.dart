import 'package:fc_inputs/models/load_bundle.dart';

class CropType {
  int id;
  String name;
  String description;
  int statusId;
  String image;
  Null deletedAt;
  String createdAt;
  String updatedAt;
  List<LoanBundle> loanBundle;
  int size;

  CropType(
      {this.id,
      this.name,
      this.description,
      this.statusId,
      this.image,
      this.deletedAt,
      this.createdAt,
      this.updatedAt,
      this.loanBundle,
      this.size});

  CropType.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    statusId = json['status_id'];
    image = json['image'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['loan_bundle'] != null) {
      loanBundle = new List<LoanBundle>();
      json['loan_bundle'].forEach((v) {
        loanBundle.add(new LoanBundle.fromJson(v));
      });
    }
    size = json['size'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['status_id'] = this.statusId;
    data['image'] = this.image;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.loanBundle != null) {
      data['loan_bundle'] = this.loanBundle.map((v) => v.toJson()).toList();
    }
    data['size'] = this.size;
    return data;
  }
}

