import 'dart:convert';

class StateModel {
  int id;
  String name;
  int countryId;
  int statusId;
  String createdAt;
  String updatedAt;
  String deletedAt;

  StateModel(
      {this.id,
      this.name,
      this.countryId,
      this.statusId,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  StateModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    countryId = json['country_id'];
    statusId = json['status_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['country_id'] = this.countryId;
    data['status_id'] = this.statusId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}

List<StateModel> stateFromJson(String jsonData) {
  final data = json.decode(jsonData)["data"];
  // print('res: ' + data.toString());
  return List<StateModel>.from(data.map((item) => StateModel.fromJson(item)));
}


String stateToJson(StateModel data) {
  final jsonData = data.toJson();
  // print('jsonData: ' + jsonData.toString());
  return json.encode(jsonData);
}
