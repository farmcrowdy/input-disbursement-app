import 'dart:math';

Random random = Random();
List names = [
  "Emmanuel Olu-Flourish",
  "Timothy Olaleke",
  "Rasheedat Abdulazeez",
  "Temilade Adelakun",
  "Micheal Mekuleyi",
  "Bukola Alada",
  "Chiamaka Nwangwu",
  "Chijioke Ofoegbu",
  "Nike Adekanmbi",
  "Oluwatobi Amosu",
  "Precious Akinkuolie",
  "Qaozara Adepeju",
  "Tonia Nwani",
  "Ugochukwu Tony",
];

List headlines = [
  "2019 mdairy Benefit Gala & Awards Ceremony",
  "2019 Nigeria Higher Education Forum",
  "3rd Annual Scholarship Essay Competition",
  "mdairy SCHOLARS 2021",
  "mdairy SCHOLARS 2020",
  "mdairy CODE-BACK CHALLENGE",
  "Meet Our 2019 Honorees: Gbenga Oyebode",
  "Meet Our 2019 Honorees: Aisha Muhammed-Oyebode",
  "Mercy Ndubueze earned placement to Bank of America",
  "Mujaheed Nuhu mdairy Scholar awarded a scholarship",
  "Al-Amin Bugaje (mdairy Scholar 16) graduation day",
  "Meet Our 2019 Honorees: Oscar N. Onyema",
];

List messages = [
  "Hey, how are you doing?",
  "Are you available tomorrow?",
  "Do you have milk available",
  "I'm currently in Lagos",
  "mdairy is a very great app",
  "What are the your current grazing patterns",
  "mdairy app is awesome",
  "Can you help make a transaction",
  "I will deliver the milk tomorrow",
  "I just delivered the milk",
  "The data log has been added to mdairy",
];

List notifs = [
  "${names[random.nextInt(10)]} and ${random.nextInt(100)} others liked your post",
  "${names[random.nextInt(10)]} liked your post",
  "${names[random.nextInt(10)]} credited you",
  "${names[random.nextInt(10)]} messaged you",
  "${names[random.nextInt(10)]} commented on your post",
  "${names[random.nextInt(10)]} commented on your post",
  "You have a new message",
  "You have a new message",
  "You have a new message",
  "You have a new message",
  "You have a new message",
];

List notifications = List.generate(
    13,
    (index) => {
          "name": names[random.nextInt(10)],
          "dp": "assets/cm.png",
          "time": "${random.nextInt(50)} min ago",
          "notif": notifs[random.nextInt(10)]
        });

List posts = List.generate(
    13,
    (index) => {
          "name": headlines[random.nextInt(10)],
          "dp": "assets/cm.png",
          "time": "${random.nextInt(50)} min ago",
          "img": "assets/cm.png"
        });

List chats = List.generate(
    13,
    (index) => {
          "name": names[random.nextInt(10)],
          "dp": "assets/cm.png",
          "msg": messages[random.nextInt(10)],
          "counter": random.nextInt(20),
          "time": "${random.nextInt(50)} min ago",
          "isOnline": random.nextBool(),
        });

List universityNames = [
  "Kano",
  "Kebbi",
  "Ibadan",
  "Abuja",
  "Maguduri",
  "Ogun",
];


List types = ["text", "image"];

List conversation = List.generate(
    10,
    (index) => {
          "username": "Group ${random.nextInt(20)}",
          "time": "${random.nextInt(50)} min ago",
          "type": types[random.nextInt(2)],
          "replyText": messages[random.nextInt(10)],
          "isMe": random.nextBool(),
          "isGroup": false,
          "isReply": random.nextBool(),
        });

List years = ["2017", "2018", "2019"];

List friends = List.generate(
    13,
    (index) => {
          "name": names[random.nextInt(10)],
          "dp": "assets/cm.png",
          "status": "Joined ${years[random.nextInt(3)]}",
          "isAccept": random.nextBool(),
        });

List eventsName = [
  "mdairy 3rd Annual Scholarship Essay Competition",
  "mdairy Leadership in Business Award 2019",
  "mdairy 2019 Nigeria Higher Education Forum",
  "mdairy CODE-BACK CHALLENGE Submission",
  "2019 mdairy Benefit Gala & Awards Ceremony",
  "mdairy Orientation Programme for Cohort 2019",
];


List scholars = [
  {
    "name": "Emmanuel Olu-Flourish",
    "school": "Kano",
    "email": "emmanuel.oluflourish@gmail.com",
    "phone": "08161691655",
    "scholar_year": "2018"
  },
  {
    "name": "Timothy Olaleke",
    "school": "Abuja",
    "email": "timothyolaleke@gmail.com",
    "phone": "08136457976",
    "scholar_year": "2017"
  },
  {
    "name": "Rasheedat Abdulazeez",
    "school": "Abuja",
    "email": "rasheedatabdulazeez131@gmail.com",
    "phone": "08120955188",
    "scholar_year": "2018"
  },
  {
    "name": "Temilade Adelakun",
    "school": "Kano",
    "email": "temiladeadelakun16@gmail.com",
    "phone": "08106991043",
    "scholar_year": "2018"
  },
  {
    "name": "Micheal Mekuleyi",
    "school": "Kano",
    "email": "mekuleyimichael@gmail.com",
    "phone": "+234 810 272 3247",
    "scholar_year": "2019"
  },
  {
    "name": "Bukola Alada",
    "school": "Kano",
    "email": "name@gmail.com",
    "phone": "08036039866",
    "scholar_year": "2018"
  },
  {
    "name": "Chiamaka Nwangwu",
    "school": "Kano",
    "email": "name@gmail.com",
    "phone": "0803 635 5827",
    "scholar_year": "2018"
  },
  {
    "name": "Chijioke Ofoegbu",
    "school": "Maguduri",
    "email": "cofoegbu003@gmail.com",
    "phone": "+234 816 077 2746",
    "scholar_year": "2018"
  },
  {
    "name": "Emmanuel Faith mdairy",
    "school": "Kano",
    "email": "name@gmail.com",
    "phone": "+234 817 864 5411",
    "scholar_year": "2018"
  },
  {
    "name": "John Olaniyi",
    "school": "Maguduri",
    "email": "name@gmail.com",
    "phone": "+234 818 648 2381",
    "scholar_year": "2018"
  },
  {
    "name": "Nike Adekanmbi",
    "school": "Kano",
    "email": "name@gmail.com",
    "phone": "+234 816 401 5718",
    "scholar_year": "2018"
  },
  {
    "name": "Oluwatobi Amosu",
    "school": "Maguduri",
    "email": "name@gmail.com",
    "phone": "0816 995 0644",
    "scholar_year": "2018"
  },
  {
    "name": "Precious Akinkuolie",
    "school": "Kano",
    "email": "name@gmail.com",
    "phone": "+234 809 303 6970",
    "scholar_year": "2018"
  },
  {
    "name": "Qaozara Adepeju",
    "school": "Maguduri",
    "email": "name@gmail.com",
    "phone": "+234 905 200 8033",
    "scholar_year": "2018"
  },
  {
    "name": "Tonia Nwani",
    "school": "Kebbi",
    "email": "nwanianthonia6@gmail.com",
    "phone": "+234 706 699 5657",
    "scholar_year": "2019"
  },
  {
    "name": "Ugochukwu Tony",
    "school": "Abuja",
    "email": "princetony3u@gmail.com",
    "phone": "+234 813 117 2993",
    "scholar_year": "2018"
  },
  {
    "name": "Humainat",
    "school": "Kano",
    "email": "name@gmail.com",
    "phone": "0802 644 6890",
    "scholar_year": "2019"
  },
  {
    "name": "Gimba Bala",
    "school": "Kano",
    "email": "name@gmail.com",
    "phone": "0802 316 3467",
    "scholar_year": "2018"
  },
  {
    "name": "Pemisin EEE UI",
    "school": "Kano",
    "email": "name@gmail.com",
    "phone": "08130521101",
    "scholar_year": "2018"
  },
  {
    "name": "Layi Arowolo",
    "school": "Kano",
    "email": "layirowolo@gmail.com",
    "phone": "08079353918",
    "scholar_year": "2018"
  },
];
