import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xFFABBA16);
  static const Color secondaryColor = Color(0xFF293737);

  // static const Color secondaryColor = Color.fromARGB(255, 246, 247, 255);

  static const Color backgroundColor = Color(0xFFF4F3F8);

  static const Color black = Color(0xFF000000);
  static const Color blackShade1 = Color(0xFF3B3870);

  static const Color white = Color(0xFFFFFFFF);
  static const Color whiteShade1 = Color(0xFFFFFFDE);

  static const Color grey = Color(0xFF999999); 
  static const Color greyShade1 = Color(0xFFEFEFEF);
  static const Color greyShade2 = Color(0xFFE4E4E4);

  static const Color red = Color(0xFFF1291A);
  static const Color redLogout = Color(0xFFEE2C25);
  static const Color green = Color(0xFF3ACC6C);

  //Tranparent
  static const Color red_transparent = Color(0xffFFF3F3);
  static const Color green_transparent = Color(0xffEEFBFA);
  static const Color blue_transparent = Color(0xffF3F3FE);

  // Cool or Light Colour
  static const Color red_light = Color(0xffFD706B);
  static const Color green_light = Color(0xFF3ACC6C);
  static const Color blue_light = Color(0xff415EF6);

  // Card Icon Colors
  static const Color card_green = Color(0xFF24B354);
  static const Color card_purple = Color(0xFFA128FF);
  static const Color card_blue = Color(0xFF1DA1F2);
}
