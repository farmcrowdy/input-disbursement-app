// import 'dart:convert';
// import 'package:dio/dio.dart';
// import 'package:fc_inputs/models/farmer.dart';
// import 'package:fc_inputs/models/state.dart';
// import 'package:http/http.dart' show Client;

// class ApiService2 {
//   final String baseUrl = "https://staging-afjp.farmcrowdy.com/api";
//   Client client = Client();

//   Response response;
//   Dio dio = new Dio();

//   // SALES INPUT
//   Future<List<StateModel>> getStates() async {
//     if (response.statusCode == 200) {
//       Response response = await Dio().get("$baseUrl/state/all");
//       // print(response);
//       print('statusCode: ' + response.statusCode.toString());
//       print('Status: ' + response.data["status"].toString());
//       print('Message: ' + response.data["message"].toString());
//       print('Data: ' + response.data["data"].toString());

//       return statesFromJson(response.data);

//       var jsonData = response.data["data"];

//       final data = json.decode(jsonData)["data"];
//       // print('res: ' + data.toString());
//       return List<StateModel>.from(
//           data.map((item) => StateModel.fromJson(item)));
//     }
//   }

//   void getHttp() async {
//     try {
//       Response response = await Dio().get("$baseUrl/state/all");
//       // print(response);
//       print('statusCode: ' + response.statusCode.toString());
//       print('Status: ' + response.data["status"].toString());
//       print('Message: ' + response.data["message"].toString());
//       print('Data: ' + response.data["data"].toString());
//     } catch (e) {
//       print(e);
//     }
//   }

//   Future<bool> registerFarmer(Farmer data) async {
//     final response = await client.post(
//       "$baseUrl/register",
//       headers: {"content-type": "application/json"},
//       // headers: {"content-type": "application/x-www-form-urlencoded"},
//       body: farmerToJson(data),
//     );
//     print('data: ' + data.toString());
//     print(response.body);
//     if (response.statusCode == 201 || response.body.contains('"status":true')) {
//       return true;
//     } else {
//       return false;
//     }
//   }

//   Future<List<Farmer>> getFarmers() async {
//     final response = await client.get("$baseUrl/api/input");
//     if (response.statusCode == 200) {
//       return farmerFromJson(response.body);
//     } else {
//       return null;
//     }
//   }

//   Future<bool> createFarmer(Farmer data) async {
//     final response = await client.post(
//       "$baseUrl/api/input/create",
//       headers: {"content-type": "application/json"},
//       body: farmerToJson(data),
//     );
//     // print(response.body);
//     if (response.statusCode == 201 || response.body.contains('"status":true')) {
//       return true;
//     } else {
//       return false;
//     }
//   }

//   Future<bool> updateFarmer(Farmer data) async {
//     // print('data:' + data.toString());
//     final response = await client.post(
//       "$baseUrl/api/input/edit/${data.id}",
//       headers: {"content-type": "application/json"},
//       body: farmerToJson(data),
//     );
//     print(data.id);
//     print('response: ' + response.body.toString());
//     if (response.statusCode == 200) {
//       return true;
//     } else {
//       return false;
//     }
//   }

//   Future<bool> deleteFarmer(int id) async {
//     final response = await client.delete(
//       "$baseUrl/api/input/delete/$id",
//       headers: {"content-type": "application/json"},
//     );
//     if (response.statusCode == 200) {
//       return true;
//     } else {
//       return false;
//     }
//   }
// }
