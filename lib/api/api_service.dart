import 'dart:convert';

import 'package:fc_inputs/models/crop_type.dart';
import 'package:fc_inputs/models/farmer.dart';
import 'package:fc_inputs/models/loan.dart';
import 'package:fc_inputs/models/loan_disbursed.dart';
import 'package:fc_inputs/models/user.dart';
import 'package:fc_inputs/models/state.dart';
import 'package:http/http.dart' show Client;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences prefs;

class ApiService {
  // final String baseUrl = "https://staging-afjp.farmcrowdy.com/api";
  final String baseUrl = "https://afjp.farmcrowdy.com/api";
  Client client = Client();

  Future<List<StateModel>> getStates() async {
    final response = await client.get("$baseUrl/state/all");
    if (response.statusCode == 200) {
      print('response: ' + response.toString());
      print('response.body: ' + response.body);
      return stateFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<String> getStateById(int id) async {
    final response = await client.get("$baseUrl/state/get/$id");
    if (response.statusCode == 200) {
      print('response.body: ' + response.body);
      var jsonResponse = json.decode(response.body);
      return jsonResponse["data"]["name"];
    } else {
      return null;
    }
  }

  Future register(User user) async {
    Map<String, String> requestBody = user.toJson();
    Map<String, String> headers = <String, String>{
      'Authorization': 'Basic ${base64Encode(utf8.encode('user:password'))}'
    };
    var uri = Uri.parse("$baseUrl/register");
    var request = http.MultipartRequest('POST', uri)
      // ..headers.addAll(headers)
      ..fields.addAll(requestBody);
    var response = await request.send();
    final respStr = await response.stream.bytesToString();
    print(respStr);

    if (response.statusCode == 200 && respStr.contains('status":true')) {
      return true;
      // } else if (status == 'false') {
      //   return responseJson["message"].toString();
    } else {
      return null;
    }
  }

  Future updateUser(User user) async {
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('FC_INPUTS_TOKEN');

    Map<String, String> requestBody = {
      "first_name": "${user.firstName}",
      "last_name": "${user.lastName}",
      "phone_number": "${user.phoneNumber}",
      "farm_state_id": "${user.farmStateId}",
    };
    Map<String, String> headers = <String, String>{
      "Authorization": "Bearer $token"
    };
    var uri = Uri.parse("$baseUrl/user/profile");
    var request = http.MultipartRequest('POST', uri)
      ..headers.addAll(headers)
      ..fields.addAll(requestBody);
    var response = await request.send();

    final respStr = await response.stream.bytesToString();
    print(respStr);

    if (response.statusCode == 200 && respStr.contains('status":true')) {
      // Update user information in sharedpreference
      prefs = await SharedPreferences.getInstance();
      String state = await getStateById(int.parse(user.farmStateId));
      await prefs.setString('FC_INPUTS_FIRST_NAME', user.firstName);
      await prefs.setString('FC_INPUTS_LAST_NAME', user.lastName);
      await prefs.setString('FC_INPUTS_PHONE', user.phoneNumber);
      await prefs.setString('FC_INPUTS_STATE_ID', user.farmStateId);
      await prefs.setString('FC_INPUTS_STATE_NAME', state);

      return true;
      // } else if (status == 'false') {
      //   return responseJson["message"].toString();
    } else {
      return null;
    }
  }

  Future<bool> login(String phone, String password) async {
    final response = await client.post(
      "$baseUrl/login",
      body: {
        "phone_number": "$phone",
        "password": "$password",
        "request_type": "login"
      },
    );

    var responseJson = json.decode(response.body);
    String status = responseJson["status"].toString();

    if (response.statusCode == 200 && status == 'true') {
      String token = responseJson["data"]["access_token"];
      User user = User.fromJson(responseJson["data"]["user"]);

      print(token);

      // Store token and user information
      prefs = await SharedPreferences.getInstance();
      String state = await getStateById(int.parse(user.farmStateId));
      await prefs.setString('FC_INPUTS_TOKEN', token);
      await prefs.setString('FC_INPUTS_FIRST_NAME', user.firstName);
      await prefs.setString('FC_INPUTS_LAST_NAME', user.lastName);
      // await prefs.setString('FC_INPUTS_PHONE', user.phoneNumber);
      await prefs.setString('FC_INPUTS_PHONE', phone);
      await prefs.setString('FC_INPUTS_STATE_ID', user.farmStateId);
      await prefs.setString('FC_INPUTS_STATE_NAME', state);
      await prefs.setString('FC_INPUTS_ROLE_ID', user.roleId);

      // String storedToken = prefs.getString('FC_INPUTS_TOKEN');
      print(prefs.getString('FC_INPUTS_TOKEN'));
      print(prefs.getString('FC_INPUTS_FIRST_NAME'));
      print(prefs.getString('FC_INPUTS_LAST_NAME'));
      print(prefs.getString('FC_INPUTS_PHONE'));
      print(prefs.getString('FC_INPUTS_STATE_ID'));
      print(prefs.getString('FC_INPUTS_STATE_NAME'));
      print(prefs.getString('FC_INPUTS_ROLE_ID'));

      return true;
    } else {
      return false;
    }
  }

  Future getFarmerByPhone(String phone) async {
    phone = "08100927568"; // For testing
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('FC_INPUTS_TOKEN');

    final response = await client.get(
      "$baseUrl/farmer/get/$phone",
      headers: {
        "content-type": "application/json",
        "Authorization": "Bearer $token"
      },
    );
    // print(response.statusCode);
    print('response.body: ' + response.body);

    var responseJson = json.decode(response.body);
    String status = responseJson["status"].toString();
    Farmer farmer = Farmer.fromJson(responseJson["data"]);

    if (response.statusCode == 200 && status == 'true') {
      return farmer;
    } else if (status == 'false') {
      return responseJson["message"].toString();
    } else {
      return null;
    }
  }

  Future<List<CropType>> getCropTypes() async {
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('FC_INPUTS_TOKEN');

    // final response = await client.get("$baseUrl/state/all");
    final response = await client.get(
      "$baseUrl/croptype/all",
      headers: {
        "content-type": "application/json",
        "Authorization": "Bearer $token"
      },
    );

    if (response.statusCode == 200) {
      print('response: ' + response.toString());
      print('response.body: ' + response.body);

      final data = json.decode(response.body)["data"]["data"];
      return List<CropType>.from(data.map((item) => CropType.fromJson(item)));
    } else {
      return null;
    }
  }

  Future<List<Loan>> getAllLoans({bool isTodayOnly}) async {
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('FC_INPUTS_TOKEN');

    final response = await client.get(
      "$baseUrl/farmer/loan/all",
      headers: {
        "content-type": "application/json",
        "Authorization": "Bearer $token"
      },
    );

    if (response.statusCode == 200) {
      print('response: ' + response.toString());
      print('response.body: ' + response.body);

      final data = json.decode(response.body)["data"]["loans"]["data"];

      if (!isTodayOnly) {
        return List<Loan>.from(data.map((item) => Loan.fromJson(item)));
      } else {
        List<Loan> loans = new List<Loan>();
        data.forEach((v) {
          Loan loan = new Loan.fromJson(v);
          // Filter Loan Disbursed TOday
          if (DateTime.now().toString().substring(0, 10) ==
              loan.createdAt.substring(0, 10)) {
            loans.add(loan);
          }
        });

        return loans;
      }
    } else {
      return null;
    }
  }

  Future<List<LoanDisbursed>> getLoansDisbursedToday() async {
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('FC_INPUTS_TOKEN');

    final response = await client.get(
      "$baseUrl/user/farmers/disbursed/today",
      headers: {
        "content-type": "application/json",
        "Authorization": "Bearer $token"
      },
    );

    if (response.statusCode == 200) {
      print('response: ' + response.toString());
      print('response.body: ' + response.body);

      final data = json.decode(response.body)["data"];
      return List<LoanDisbursed>.from(data.map((item) => LoanDisbursed.fromJson(item)));
    } else {
      return null;
    }
  }

Future<List<LoanDisbursed>> getLoansDisbursed() async {
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('FC_INPUTS_TOKEN');

    final response = await client.get(
      "$baseUrl/user/farmers/disbursed",
      headers: {
        "content-type": "application/json",
        "Authorization": "Bearer $token"
      },
    );

    if (response.statusCode == 200) {
      print('response: ' + response.toString());
      print('response.body: ' + response.body);

      final data = json.decode(response.body)["data"];
      return List<LoanDisbursed>.from(data.map((item) => LoanDisbursed.fromJson(item)));
    } else {
      return null;
    }
  }


  Future<List<Loan>> getLoansByFarmerId(int farmerId) async {
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('FC_INPUTS_TOKEN');

    final response = await client.get(
      "$baseUrl/farmer/loan/get_by_farmer_id/$farmerId",
      headers: {
        "content-type": "application/json",
        "Authorization": "Bearer $token"
      },
    );

    if (response.statusCode == 200) {
      print('response: ' + response.toString());
      print('response.body: ' + response.body);

      final data = json.decode(response.body)["data"]["loan_records"];
      return List<Loan>.from(data.map((item) => Loan.fromJson(item)));
    } else {
      return null;
    }
  }

  Future createFeedback(String phone, String feedback) async {
    phone = "08100927568"; // For testing
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('FC_INPUTS_TOKEN');

    Map<String, String> requestBody = {
      "farmer_phone": "$phone",
      "feedback_text": "$feedback",
    };
    Map<String, String> headers = <String, String>{
      "Authorization": "Bearer $token"
    };
    var uri = Uri.parse("$baseUrl/feedback/create");
    var request = http.MultipartRequest('POST', uri)
      ..headers.addAll(headers)
      ..fields.addAll(requestBody);
    var response = await request.send();

    final respStr = await response.stream.bytesToString();
    print(respStr);

    if (response.statusCode == 200 && respStr.contains('status":true')) {
      return true;
      // } else if (status == 'false') {
      //   return responseJson["message"].toString();
    } else {
      return null;
    }
  }

  Future<List<dynamic>> getDisbursedMetric() async {
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('FC_INPUTS_TOKEN');

    final response = await client.get(
      "$baseUrl/user/dashboard/summary",
      headers: {
        "content-type": "application/json",
        "Authorization": "Bearer $token"
      },
    );

    if (response.statusCode == 200) {
      print('response: ' + response.toString());
      print('response.body: ' + response.body);

      final data = json.decode(response.body)["data"];
      List<dynamic> res = [data["loansDisbursed"], data["loansDisbursedToday"]];
      print(res[0].toString() + ' asda ' + res[1].toString());
      return res;
    } else {
      return [0, 0];
    }
  }

  Future createLoan(Farmer farmer, List<int> loanBundles) async {
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('FC_INPUTS_TOKEN');

    Map<String, String> requestBody = {
      "status_id": "2",
      "farmer_id": "${farmer.id}",
      "loan_bundles": "${loanBundles.toString()}",
    };
    Map<String, String> headers = <String, String>{
      "Authorization": "Bearer $token"
    };
    var uri = Uri.parse("$baseUrl/farmer/loan/create");
    var request = http.MultipartRequest('POST', uri)
      ..headers.addAll(headers)
      ..fields.addAll(requestBody);
    var response = await request.send();
    print(response);
    final respStr = await response.stream.bytesToString();
    print(respStr);
  }
}
