import 'package:fc_inputs/utils/colors.dart';
import 'package:flutter/material.dart';

class NotificationItem extends StatelessWidget {
  final String name;
  final String time;
  final String msg;
  final Function onTap;

  NotificationItem({
    Key key,
    @required this.name,
    @required this.time,
    @required this.msg,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: InkWell(
        onTap: onTap,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 36.0,
              height: 36.0,
              decoration: BoxDecoration(
                color: AppColors.grey,
                shape: BoxShape.circle,
              ),
              alignment: Alignment.center,
              child: Text(
                'FC',
                style: TextStyle(
                    color: AppColors.white, fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(width: 16.0),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        name,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 14.0),
                      ),
                      Text(
                        time,
                        style: TextStyle(
                          fontWeight: FontWeight.w300,
                          color: AppColors.grey,
                          fontSize: 14,
                        ),
                      ),
                    ],
                  ),
                
                SizedBox(height: 8.0),
                      Text(
                        msg,
                        style: TextStyle(color: Colors.black54),
                      )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
