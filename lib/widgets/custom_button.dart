import 'package:fc_inputs/utils/colors.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  CustomButton({
    @required this.title,
    this.onTap,
    this.isFilled,
  });

  final VoidCallback onTap;
  final String title;
  final bool isFilled;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        children: <Widget>[
          InkWell(
            onTap: onTap,
            child: Container(
              height: 50.0,
              width: double.infinity,
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 24.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: isFilled ? AppColors.primaryColor:AppColors.white,
                border: Border.all(color: AppColors.primaryColor, width: 1.0),
              ),
              child: Center(
                child: Text(title,
                    style: TextStyle(
                        color: isFilled ? AppColors.white : AppColors.primaryColor,
                        fontWeight: FontWeight.bold)),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
