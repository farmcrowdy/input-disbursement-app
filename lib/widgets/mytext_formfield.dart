import 'package:fc_inputs/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyTextFormField extends StatelessWidget {
  final String title;
  final String hintText;
  final String helperText;
  final String labelText;
  final String value;
  final int maxLines;
  final int maxLength;
  final Function validator;
  final Function onSaved;
  final Function onTap;
  final TextEditingController controller;
  final TextInputType textInputType;
  final bool isPassword;
  final bool isReadOnly;
  final bool showCounter;
  final Widget prefixIcon;
  final Widget suffixIcon;
  MyTextFormField({
    this.title = '',
    this.hintText,
    this.helperText,
    this.labelText,
    this.value,
    this.maxLines = 1,
    this.maxLength = -1,
    this.validator,
    this.onSaved,
    this.onTap,
    this.controller,
    this.textInputType = TextInputType.text,
    this.isPassword = false,
    this.isReadOnly = false,
    this.showCounter = false,
    this.prefixIcon,
    this.suffixIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 14.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          title.isEmpty
              ? Container()
              : Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(
                    title,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
                  ),
                ),
          TextFormField(
            onTap: onTap,
          
            controller: controller,
            style: GoogleFonts.inter(textStyle: TextStyle(fontSize: 14.0)),
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            readOnly: isReadOnly,
            initialValue: value,
            maxLines: maxLines,
            textCapitalization: textInputType == TextInputType.emailAddress
                ? TextCapitalization.none
                : TextCapitalization.sentences,
            maxLength: (maxLength == -1) ? null : maxLength,
            decoration: InputDecoration(
              labelText: labelText,
              helperText: helperText,
              counterText: showCounter ? null : '',
              suffixIcon: suffixIcon,
              prefixIcon: prefixIcon,
              hintText: hintText,
              contentPadding: EdgeInsets.all(15.0),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4.0),
                borderSide: const BorderSide(color: Colors.grey, width: 0.0),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4.0),
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
              ),
              filled: true,
              fillColor: Colors.white,
            ),
            obscureText: isPassword ? true : false,
            validator: validator,
            onSaved: onSaved,
            keyboardType: textInputType,
          ),
        ],
      ),
    );
  }
}
