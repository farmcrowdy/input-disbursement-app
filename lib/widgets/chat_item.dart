import 'package:fc_inputs/utils/colors.dart';
import 'package:flutter/material.dart';

class ChatItem extends StatelessWidget {
  final String dp;
  final String name;
  final String time;
  final String msg;
  final Function onTap;

  ChatItem({
    Key key,
    this.dp,
    @required this.name,
    @required this.time,
    @required this.msg,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical:16.0),
      child: Column(
        children: <Widget>[
          InkWell(
            onTap: onTap,
                      child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                CircleAvatar(
                  child: Image(image: AssetImage(dp)),
                  radius: 30.0,
                ),
                SizedBox(width: 16.0),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        name,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15.0),
                      ),
                      SizedBox(height: 8.0),
                      Text(msg, style: TextStyle(color: Colors.black54))
                    ],
                  ),
                ),
                Text(
                  time,
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 11,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
