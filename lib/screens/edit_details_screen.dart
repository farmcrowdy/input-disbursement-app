import 'package:fc_inputs/screens/home_screen.dart';
import 'package:fc_inputs/widgets/loading.dart';
import 'package:fc_inputs/widgets/toast_text.dart';
import 'package:flutter/material.dart';
import 'package:fc_inputs/api/api_service.dart';
import 'package:fc_inputs/models/user.dart';
import 'package:fc_inputs/models/state.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/custom_button.dart';
import 'package:fc_inputs/widgets/mytext_formfield.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:google_fonts/google_fonts.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class EditDetailsScreen extends StatefulWidget {
  EditDetailsScreen({Key key, this.title, this.user}) : super(key: key);

  final User user;
  final String title;

  @override
  EditDetailsScreenState createState() => EditDetailsScreenState();
}

class EditDetailsScreenState extends State<EditDetailsScreen> {
  User user = new User();

  bool _isLoading = false;
  ApiService _apiService = ApiService();
  final _formKey = GlobalKey<FormState>();

  String _selectedState = '';
  String _selectedStateId;

  String state = "";

  void showModal(context) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(8),
            child: FutureBuilder(
              future: _apiService.getStates(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<StateModel>> snapshot) {
                if (snapshot.hasError) {
                  return Center(
                    child: Text(
                        "Something wrong with message: ${snapshot.error.toString()}"),
                  );
                } else if (snapshot.connectionState == ConnectionState.done) {
                  List<StateModel> salesInputs = snapshot.data;
                  print('salesInputs: ' + snapshot.data.toString());
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 4.0, horizontal: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text(
                            'Select Farm Location',
                            style: TextStyle(
                                fontSize: 18.0, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Expanded(
                          child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: salesInputs.length,
                            itemBuilder: (context, index) {
                              StateModel salesInput = salesInputs[index];
                              return InkWell(
                                child: Container(
                                  padding: EdgeInsets.all(16.0),
                                  child: Text(salesInput.name),
                                ),
                                onTap: () {
                                  setState(() {
                                    _selectedState = salesInput.name;
                                    _selectedStateId = salesInput.id.toString();
                                  });
                                  Navigator.of(context).pop();
                                },
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    // Initiate FarmStateId\
    String _selectedState = widget.user.farmStateName;
    String _selectedStateId = widget.user.farmStateId;

    return Scaffold(
      key: _scaffoldState,

      appBar: AppBar(
        title: Text(
          'Edit Details',
          style: GoogleFonts.inter(
              textStyle: TextStyle(color: AppColors.secondaryColor)),
        ),
        centerTitle: true,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 20.0),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 12.0),
                    MyTextFormField(
                      labelText: 'Phone number',
                      hintText: 'Phone number',
                      textInputType: TextInputType.number,
                      value: widget.user.phoneNumber,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter phone number';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        user.phoneNumber = value;
                      },
                    ),
                    MyTextFormField(
                      labelText: 'First name',
                      hintText: 'First name',
                      value: widget.user.firstName,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter first name';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        user.firstName = value;
                      },
                    ),
                    MyTextFormField(
                      labelText: 'Last name',
                      hintText: 'Last name',
                      value: widget.user.lastName,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter last name';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        user.lastName = value;
                      },
                    ),
                    MyTextFormField(
                      labelText: 'Farm State Location',
                      onTap: () => showModal(context),
                      isReadOnly: true,
                      suffixIcon: Icon(Icons.keyboard_arrow_down),
                      value: widget.user.farmStateName,
                      hintText: _selectedState.isEmpty
                          ? 'Farm State Location'
                          : _selectedState,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Select farm state location';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        user.farmStateId = _selectedStateId;
                      },
                    ),
                    // MyTextFormField(
                    //   labelText: 'Password',
                    //   hintText: 'Password',
                    //   validator: (String value) {
                    //     if (value.isEmpty) {
                    //       return 'Enter your password';
                    //     }
                    //     return null;
                    //   },
                    //   onSaved: (String value) {
                    //     user.password = value;
                    //   },
                    // ),
                    SizedBox(height: 8.0),
                    CustomButton(
                      title: 'Save Details',
                      isFilled: true,
                      onTap: () async {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();

                          print(user.toString());

                          _apiService.updateUser(user).then((isSuccess) {
                            setState(() => _isLoading = false);
                            if (isSuccess is bool) {
                              Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      HomeScreen(),
                                ),
                              );
                              showToastWidget(ToastText(
                                  text:
                                      'You details have been successfully updated.'));
                            } else {
                              _scaffoldState.currentState.showSnackBar(SnackBar(
                                content:
                                    Text("Unable to save details. Try again."),
                              ));
                            }
                          });
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          _isLoading ? Loading() : Container(),
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
