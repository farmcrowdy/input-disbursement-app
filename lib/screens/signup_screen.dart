import 'dart:convert';

import 'package:fc_inputs/screens/signin_screen.dart';
import 'package:fc_inputs/widgets/loading.dart';
import 'package:fc_inputs/widgets/toast_text.dart';
import 'package:flutter/material.dart';
import 'package:fc_inputs/api/api_service.dart';
import 'package:fc_inputs/models/user.dart';
import 'package:fc_inputs/models/state.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/custom_button.dart';
import 'package:fc_inputs/widgets/mytext_formfield.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';

import 'package:path/path.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:async';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

/**
 * accepts three parameters, the endpoint, formdata (except fiels),files (key,File)
 * returns Response from server
 */
Future<Response> sendForm(
    String url, Map<String, dynamic> data, Map<String, File> files) async {
  Map<String, MultipartFile> fileMap = {};
  for (MapEntry fileEntry in files.entries) {
    File file = fileEntry.value;
    String fileName = basename(file.path);
    fileMap[fileEntry.key] =
        MultipartFile(file.openRead(), await file.length(), filename: fileName);
  }
  data.addAll(fileMap);
  var formData = FormData.fromMap(data);
  Dio dio = new Dio();
  return await dio.post(url,
      data: formData, options: Options(contentType: 'multipart/form-data'));
}

/**
 * accepts two parameters,the endpoint and the file
 * returns Response from server
 */
Future<Response> sendFile(String url, File file) async {
  Dio dio = new Dio();
  var len = await file.length();
  var response = await dio.post(url,
      data: file.openRead(),
      options: Options(headers: {
        Headers.contentLengthHeader: len,
      } // set content-length
          ));
  return response;
}

class SignUpScreen extends StatefulWidget {
  SignUpScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  User user = new User();

  bool _isLoading = false;
  ApiService _apiService = ApiService();
  final _formKey = GlobalKey<FormState>();

  String _selectedState = '';
  String _selectedStateId;

  String state = "";

  final String endPoint = 'https://staging-afjp.farmcrowdy.com/api/register';

  File _image;
  File file;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    print('upload started');
    //upload image
    //scenario  one - upload image as poart of formdata
    var res1 = await sendForm(
        endPoint,
        {'first_name': 'Emmanuel', 'last_name': 'Olu-Flourish'},
        {'avatar': image});
    print("res-1 $res1");

    var res2 = await sendFile(endPoint, image);
    print("res-2: $res2");
    setState(() {
      _image = image;
    });
  }

  Future<Map<String, dynamic>> _uploadImage(File image) async {
    var header = {
      'Accept': 'application/json',
      // 'Authorization': 'Bearer $token'
    };
    setState(() {
      // isUploading = true;
    });
    // Find the mime type of the selected file by looking at the header bytes of the file
    final mimeTypeData =
        lookupMimeType(image.path, headerBytes: [0xFF, 0xD8]).split('/');
    // Intilize the multipart request
    final imageUploadRequest =
        http.MultipartRequest('POST', Uri.parse(endPoint));
    imageUploadRequest.headers.addAll(header);
    // Attach the file in the request
    final file = await http.MultipartFile.fromPath('avatar', image.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));

    // Explicitly pass the extension of the image with request body
    // Since image_picker has some bugs due which it mixes up
    // image extension with file name like this filenamejpge
    // Which creates some problem at the server side to manage
    // or verify the file extension
    imageUploadRequest.fields['ext'] = mimeTypeData[1];
    imageUploadRequest.files.add(file);

    try {
      final streamedResponse = await imageUploadRequest.send();
      final response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode != 200) {
        return null;
      }
      final Map<String, dynamic> responseData = json.decode(response.body);
      _resetState();
      return responseData;
      //print(responseData);
    } catch (e) {
      print(e);
      return null;
    }
  }

  void _startUploading() async {
    final Map<String, dynamic> response = await _uploadImage(file);
    print(response);
    // Check if any error occured
    if (response == null || response.containsKey("error")) {
      print('error');
    } else {
      // Fluttertoast.showToast(msg: 'You have successfully uploaded your Avatar', toastLength: Toast.LENGTH_LONG,
      //   timeInSecForIosWeb: 3,
      //   backgroundColor: meatUpTheme,);
      print('success');
    }
  }

  void _resetState() {
    setState(() {
      // isUploading = false;
      //file = null;
    });
  }

  void showModal(context) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(8),
            child: FutureBuilder(
              future: _apiService.getStates(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<StateModel>> snapshot) {
                if (snapshot.hasError) {
                  return Center(
                    child: Text(
                        "Something wrong with message: ${snapshot.error.toString()}"),
                  );
                } else if (snapshot.connectionState == ConnectionState.done) {
                  List<StateModel> salesInputs = snapshot.data;
                  print('salesInputs: ' + snapshot.data.toString());
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 4.0, horizontal: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text(
                            'Select Farm Location',
                            style: TextStyle(
                                fontSize: 18.0, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Expanded(
                          child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: salesInputs.length,
                            itemBuilder: (context, index) {
                              StateModel salesInput = salesInputs[index];
                              return InkWell(
                                child: Container(
                                  padding: EdgeInsets.all(16.0),
                                  child: Text(salesInput.name),
                                ),
                                onTap: () {
                                  setState(() {
                                    _selectedState = salesInput.name;
                                    _selectedStateId = salesInput.id.toString();
                                  });
                                  Navigator.of(context).pop();
                                },
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      // floatingActionButton: FloatingActionButton(
      //   // onPressed: getImage,
      //   onPressed: () async {
      //     var file = await ImagePicker.pickImage(source: ImageSource.camera);
      //     _uploadImage(file);
      //   },
      //   tooltip: 'Pick Image',
      //   child: Icon(Icons.add_a_photo),
      // ),
      appBar: AppBar(
        title: Text(
          'Create Account',
          style: GoogleFonts.inter(
              textStyle: TextStyle(color: AppColors.secondaryColor)),
        ),
        centerTitle: true,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 20.0),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 12.0),
                    Center(
                      child: Text('Create an account on Farmcrowdy Input'),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: InkWell(
                        onTap: () {},
                        child: Center(
                          child: Image(
                            image: AssetImage('assets/images/camera-add.png'),
                            width: 120.0,
                          ),
                        ),
                      ),
                    ),
                    MyTextFormField(
                      hintText: 'Phone number',
                      helperText: 'Length of phone number',
                      textInputType: TextInputType.number,
                      showCounter: true,
                      maxLength: 11,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter phone number';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        user.phoneNumber = value;
                      },
                    ),
                    MyTextFormField(
                      hintText: 'First name',
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter first name';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        user.firstName = value;
                      },
                    ),
                    MyTextFormField(
                      hintText: 'Last name',
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter last name';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        user.lastName = value;
                      },
                    ),
                    MyTextFormField(
                      hintText: 'Password',
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter password';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        user.password = value;
                      },
                    ),
                    MyTextFormField(
                      hintText: 'Confirm Password',
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter password again';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        user.passwordConfirmation = value;
                      },
                    ),
                    MyTextFormField(
                      onTap: () => showModal(context),
                      isReadOnly: true,
                      suffixIcon: Icon(Icons.keyboard_arrow_down),
                      hintText: _selectedState.isEmpty
                          ? 'Farm State Location'
                          : _selectedState,
                      // validator: (String value) {
                      //   if (value.isEmpty) {
                      //     return 'Select farm state location';
                      //   }
                      //   return null;
                      // },
                      // value: _selectedStateId.toString(),
                      onSaved: (value) {
                        user.farmStateId = _selectedStateId;
                      },
                    ),
                    SizedBox(height: 8.0),
                    Text(
                      'By signing up, you are agreeing to our privacy policy and terms of service.',
                      style: TextStyle(
                        color: AppColors.grey,
                        fontSize: 12.0,
                      ),
                    ),
                    SizedBox(height: 16.0),
                    CustomButton(
                      title: 'Sign Up',
                      isFilled: true,
                      onTap: () async {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
                          user.roleId = "2";
                          // print("uesr: "+user.toString());

                          if (user.password != user.passwordConfirmation) {
                            _scaffoldState.currentState.showSnackBar(SnackBar(
                              content: Text(
                                  "Your passwords don't match. Check and try again!"),
                            ));
                          } else {
                            _apiService.register(user).then((isSuccess) {
                              setState(() => _isLoading = false);
                              if (isSuccess is bool) {
                                // Navigator.pop(_scaffoldState.currentState.context);
                                Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        SignInScreen(),
                                  ),
                                );
                                showToastWidget(ToastText(
                                    text:
                                        'Account created successfully.\nYou can now Log in.'));
                              } else {
                                _scaffoldState.currentState
                                    .showSnackBar(SnackBar(
                                  content: Text("Unable to create new account"),
                                ));
                              }
                            });
                          }
                          // Navigator.of(context).pop();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          _isLoading ? Loading() : Container(),
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
