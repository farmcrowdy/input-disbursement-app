import 'package:expandable/expandable.dart';
import 'package:fc_inputs/api/api_service.dart';
import 'package:fc_inputs/models/farmer.dart';
import 'package:fc_inputs/models/load_bundle.dart';
import 'package:fc_inputs/models/loan.dart';
import 'package:fc_inputs/screens/select_crop_type_screen.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class ConfirmFarmerResultScreen extends StatefulWidget {
  ConfirmFarmerResultScreen({Key key, this.state, this.farmer})
      : super(key: key);
  final Farmer farmer;
  final String state;

  @override
  _ConfirmFarmerResultScreenState createState() =>
      _ConfirmFarmerResultScreenState();
}

class _ConfirmFarmerResultScreenState extends State<ConfirmFarmerResultScreen> {
  final currency = new NumberFormat.currency(symbol: "\₦ ");

  ApiService _apiService = ApiService();
  String capitalize(String string) {
    if (string == null) {
      throw ArgumentError("string: $string");
    }

    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }

  @override
  Widget build(BuildContext context) {
    var textStyle2 = TextStyle(color: AppColors.grey, height: 1.5);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Confirm Farmer',
          style: GoogleFonts.inter(
              textStyle: TextStyle(color: AppColors.secondaryColor)),
        ),
        centerTitle: true,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 32.0),
        child: Column(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 20.0),
                Text('Step 1 of 5', style: TextStyle(color: AppColors.grey)),
                SizedBox(height: 16.0),
                Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 24.0),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0)),
                    border: Border.all(
                      width: 0.0,
                      color: AppColors.grey,
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Farmer Details',
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.bold)),
                      SizedBox(height: 18.0),
                      Text(
                          'Name: ${capitalize(widget.farmer.firstName)} ${capitalize(widget.farmer.lastName)}',
                          style: TextStyle(
                              fontSize: 14.0, fontWeight: FontWeight.w600)),
                      SizedBox(height: 14.0),
                      Text(
                          'Farm Location:      ${capitalize(widget.state)} State',
                          style:
                              TextStyle(fontSize: 13.0, color: AppColors.grey)),
                      SizedBox(height: 10.0),
                      Text(
                          'Crop Proficiency:  ${capitalize(widget.farmer.crop)}',
                          style:
                              TextStyle(fontSize: 13.0, color: AppColors.grey)),
                    ],
                  ),
                ),
              ],
            ), 
            Expanded(
              child: Column(
                children: <Widget>[
                  Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10.0),
                            bottomRight: Radius.circular(10.0)),
                        border: Border.all(
                          width: 0.0,
                          color: AppColors.grey,
                        )),
                    child: ExpandablePanel(
                      iconColor: AppColors.primaryColor,
                      header: Container(
                        margin: const EdgeInsets.only(top: 10.0),
                        child: Text('Previous Collected Loans',
                            style: TextStyle(
                                fontSize: 16.0, fontWeight: FontWeight.bold)),
                      ),
                      expanded: Container(
                        alignment: Alignment.topCenter,
                          height: MediaQuery.of(context).size.height - 450,
                          // height: MediaQuery.of(context).size.height - 470,
                          child: FutureBuilder(
                            future: _apiService
                                .getLoansByFarmerId(widget.farmer.id),
                            builder: (BuildContext context,
                                AsyncSnapshot<List<Loan>> snapshot) {
                              if (snapshot.hasError) {
                                return Center(
                                  child: Text(
                                      "Something wrong with message: ${snapshot.error.toString()}"),
                                );
                              } else if (snapshot.connectionState ==
                                  ConnectionState.done) {
                                List<Loan> loans = snapshot.data;

                                if (loans.length > 0) {
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 4.0, horizontal: 8.0),
                                    child: ListView.separated(
                                      scrollDirection: Axis.vertical,
                                      shrinkWrap: true,
                                      reverse: true,
                                      physics: ClampingScrollPhysics(),
                                      itemCount: loans.length,
                                      itemBuilder: (context, index) {
                                        Loan loan = loans[index];

                                        var textStyle2 = TextStyle(
                                            fontSize: 13.0,
                                            color: AppColors.grey,
                                            height: 1.5);

                                        return Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            SizedBox(height: 10.0),
                                            Row(
                                              children: <Widget>[
                                                Icon(Icons.lens,
                                                    size: 8.0,
                                                    color:
                                                        AppColors.primaryColor),
                                                SizedBox(width: 16.0),
                                                Expanded(
                                                  child: Text(loan.createdAt.substring(0,10),
                                                      style: TextStyle(
                                                          fontSize: 13.0,
                                                          fontWeight:
                                                              FontWeight.bold)),
                                                ),
                                                Text(
                                                  // '\₦ ${loanBundle.price}',
                                                  currency.format(loan.amount),
                                                  style: TextStyle(
                                                      color: AppColors.grey,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 10.0),
                                            Column(
                                              children: <Widget>[
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    SizedBox(width: 24.0),
                                                    Builder(
                                                      builder: (context) {
                                                        print(loan.loanBundles.length.toString());
                                                        return Expanded(
                                                          child:
                                                              ListView.builder(
                                                                  shrinkWrap: true,
                                                                  itemCount: loan.loanBundles.length,
                                                                  itemBuilder:
                                                                      (context, index) {
                                                                  LoanBundle loanBundle = loan.loanBundles[index];
                                                                    // print(loanBundle.name);
                                                                  //  return Text(loanBundle.name);
                                                                    // return Text("-  ${loanBundle.name} (${currency.format(loanBundle.price)})",style:textStyle2);
                                                                    return Text("-  ${loanBundle.name}",style:textStyle2);
                                                                  }),
                                                        );
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ],
                                        );

                                        return buildPreviousCollectedLoan(loan);
                                      },
                                      separatorBuilder:
                                          (BuildContext context, int index) {
                                        return Padding(
                                          padding: const EdgeInsets.only(
                                              top: 12.0, bottom: 6.0),
                                          child: Align(
                                            alignment: Alignment.center,
                                            child: Container(
                                              height: 0.5,
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  1.3,
                                              child: Divider(),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  );
                                } else {
                                  return Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "No Previously Collected Loans",
                                      style: TextStyle(color: AppColors.grey),
                                    ),
                                  );
                                }
                              } else {
                                return Center(
                                    child: Padding(
                                  padding: const EdgeInsets.all(32.0),
                                  child: CircularProgressIndicator(),
                                ));
                              }
                            },
                          )),
                    ),
                  ),
                ],
              ),
            ),
            CustomButton(
              title: 'Proceed to Crop Types',
              isFilled: true,
              onTap: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      SelectCropTypeScreen(farmer: widget.farmer),
                ),
              ),
            ),
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  buildPreviousCollectedLoan(loan) {
    var textStyle2 = TextStyle(color: AppColors.grey, height: 1.5);

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 10.0),
        Row(
          children: <Widget>[
            Icon(Icons.lens, size: 8.0, color: AppColors.primaryColor),
            SizedBox(width: 16.0),
            Text('AFJP Batch A Loan',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold)),
          ],
        ),
        SizedBox(height: 10.0),
        Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 24.0),
                Text('Maize',
                    style: TextStyle(
                        color: AppColors.secondaryColor, height: 1.5)),
                SizedBox(width: 16.0),
                Column(
                  children: <Widget>[
                    Text('- Farm mechanisation (3,500)', style: textStyle2),
                    Text('- Farm mechanisation (3,500)', style: textStyle2),
                    Text('- Farm mechanisation (3,500)', style: textStyle2),
                    Text('- Farm mechanisation (3,500)', style: textStyle2),
                  ],
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
