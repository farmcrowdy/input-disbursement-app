import 'package:fc_inputs/api/api_service.dart';
import 'package:fc_inputs/models/crop_type.dart';
import 'package:fc_inputs/models/farmer.dart';
import 'package:fc_inputs/screens/select_bundle_screen.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:pin_code_text_field/pin_code_text_field.dart';

class SelectCropTypeScreen extends StatefulWidget {
  SelectCropTypeScreen({Key key, this.title, this.farmer}) : super(key: key);

  final Farmer farmer;
  final String title;

  @override
  _SelectCropTypeScreenState createState() => _SelectCropTypeScreenState();
}

class _SelectCropTypeScreenState extends State<SelectCropTypeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  TextEditingController controller = TextEditingController(text: "");
  String thisText = "";
  bool hasError = false;
  String errorMessage;

  bool _isLoading = false;
  ApiService _apiService = ApiService();

  String fcpName;
  int fcpSize;
  int fcpIndex;

  String capitalize(String string) {
    if (string == null) {
      throw ArgumentError("string: $string");
    }

    if (string.isEmpty) {
      return string;
    }
    return string[0].toUpperCase() + string.substring(1);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var textStyle2 = TextStyle(color: AppColors.grey, height: 1.5);
    return Scaffold(
      key: _scaffoldState,
      backgroundColor: AppColors.white,
      appBar: AppBar(
        title: Text(
          'Find Crop Fit',
          style: GoogleFonts.inter(
              textStyle: TextStyle(color: AppColors.secondaryColor)),
        ),
        centerTitle: true,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 32.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 20.0),
            Text('Step 2 of 5', style: TextStyle(color: AppColors.grey)),
            SizedBox(height: 16.0),
            Text('Select Crop Type',
                style: TextStyle(
                    color: AppColors.secondaryColor,
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold)),
            SizedBox(height: 12.0),
            Text(
                'Select a Crop Type from the crop types with available bundldes.',
                style: TextStyle(
                  color: AppColors.grey,
                  fontSize: 13.0, height: 1.5
                )),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 20.0),
                Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0),
                    ),
                    border: Border.all(
                      width: 0.0,
                      color: AppColors.grey,
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Farmer\'s Crop Proficiency',
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.bold)),
                      SizedBox(height: 18.0),
                      Stack(
                        children: [
                          CropTypeItem(
                            crop: capitalize(widget.farmer.crop),
                            size: 0,
                            hasSeparator: false,
                            onTap: () {
                               _scaffoldState.currentState
                                        .showSnackBar(SnackBar(
                                      content: Text(
                                          "No bundles. Select from other available crops"),
                                    ));
                            },
                          ),
                          FutureBuilder(
                            future: _apiService.getCropTypes(),
                            builder: (BuildContext context,
                                AsyncSnapshot<List<CropType>> snapshot) {
                              if (snapshot.hasError) {
                                return Center(
                                  child: Text(
                                      "Something wrong with message: ${snapshot.error.toString()}"),
                                );
                              } else if (snapshot.connectionState ==
                                  ConnectionState.done) {
                                List<CropType> cropTypes = snapshot.data;

                                return Container(
                                  color: AppColors.white,
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 4.0),
                                  child: ListView.builder(
                                    scrollDirection: Axis.vertical,
                                    shrinkWrap: true,
                                    itemCount: cropTypes.length,
                                    itemBuilder: (context, index) {
                                      CropType cropType = cropTypes[index];
                                      if (cropType.name.toLowerCase() ==
                                          widget.farmer.crop.toLowerCase()) {
                                        return CropTypeItem(
                                          crop: capitalize(cropType.name),
                                          size: cropType.size,
                                          hasSeparator: false,
                                          onTap: () {
                                            Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (BuildContext
                                                          context) =>
                                                      SelectBundleScreen(
                                                          farmer: widget.farmer,
                                                          cropType: cropType)),
                                            );
                                          },
                                        );
                                      } else {
                                        return Container();
                                      }
                                    },
                                  ),
                                );
                              } else {
                                return Center(
                                  child: Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 10.0,
                                          horizontal: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              3),
                                      color: AppColors.white,
                                      child: CircularProgressIndicator()),
                                );
                              }
                            },
                          ),
                        
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                margin: EdgeInsets.only(bottom: 16.0),
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0)),
                    border: Border.all(
                      width: 0.0,
                      color: AppColors.grey,
                    )),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 8.0),
                    Text('Other Available Crops',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold)),
                    SizedBox(height: 16.0),
                    Expanded(
                      child: FutureBuilder(
                        future: _apiService.getCropTypes(),
                        builder: (BuildContext context,
                            AsyncSnapshot<List<CropType>> snapshot) {
                          if (snapshot.hasError) {
                            return Center(
                              child: Text(
                                  "Something wrong with message: ${snapshot.error.toString()}"),
                            );
                          } else if (snapshot.connectionState ==
                              ConnectionState.done) {
                            List<CropType> cropTypes = snapshot.data;


                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 4.0),
                              child: ListView.builder(
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                itemCount: cropTypes.length,
                                itemBuilder: (context, index) {
                                  CropType cropType = cropTypes[index];

                                  if (cropType.name.toLowerCase() ==
                                          widget.farmer.crop.toLowerCase() ||
                                      cropType.size == 0) {
                                    return Container();
                                  } else {
                                    // return buildCropType(cropType);

                                    return CropTypeItem(
                                      crop: capitalize(cropType.name),
                                      size: cropType.size,
                                      onTap: () {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  SelectBundleScreen(
                                                      farmer: widget.farmer,
                                                      cropType: cropType)),
                                        );
                                      },
                                    );
                                  }
                                },
                              ),
                            );
                          } else {
                            return Center(child: CircularProgressIndicator());
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CropTypeItem extends StatelessWidget {
  const CropTypeItem({
    Key key,
    @required this.size,
    @required this.crop,
    this.onTap,
    this.hasSeparator = true,
  }) : super(key: key);

  final int size;
  final String crop;
  final Function onTap;
  final bool hasSeparator;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Column(
        children: [
          Row(
            children: <Widget>[
              Image(
                image: AssetImage('assets/images/maize.png'),
                width: 60.0,
              ),
              SizedBox(width: 16.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(crop,
                        style: TextStyle(
                            color: AppColors.secondaryColor,
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold)),
                    SizedBox(height: 8.0),
                    Text('$size bundles available',
                        style:
                            TextStyle(fontSize: 13.0, color: AppColors.grey)),
                  ],
                ),
              ),
              Icon(Icons.chevron_right, color: AppColors.primaryColor),
            ],
          ),
          hasSeparator
              ? Padding(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 10),
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      height: 0.5,
                      width: MediaQuery.of(context).size.width / 1.3,
                      child: Divider(),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
