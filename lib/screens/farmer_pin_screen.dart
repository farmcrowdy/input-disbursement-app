import 'package:fc_inputs/api/api_service.dart';
import 'package:fc_inputs/models/farmer.dart';
import 'package:fc_inputs/screens/farmer_success_screen.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/custom_button.dart';
import 'package:fc_inputs/widgets/loading.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_pin_code_fields/flutter_pin_code_fields.dart';

class FarmerPinScreen extends StatefulWidget {
  FarmerPinScreen({Key key, this.title, this.farmer, this.loanBundlesIds})
      : super(key: key);
  final Farmer farmer;
  final String title;
  final List<int> loanBundlesIds;

  @override
  FarmerPinScreenState createState() => FarmerPinScreenState();
}

class FarmerPinScreenState extends State<FarmerPinScreen> {
  ApiService _apiService = ApiService();
  TextEditingController newTextEditingController = TextEditingController();
  FocusNode focusNode = FocusNode();

  bool _isLoading = false;

  String inputPin;
  bool isPinInvalid = false;

  @override
  void dispose() {
    newTextEditingController.dispose();
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: AppBar(
        title: Text(
          'Enter Pin',
          style: GoogleFonts.inter(
              textStyle: TextStyle(color: AppColors.secondaryColor)),
        ),
        centerTitle: true,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 32.0),
            child: ListView(
              children: <Widget>[
                SizedBox(height: 20.0),
                Text('Step 4 of 5', style: TextStyle(color: AppColors.grey)),
                SizedBox(height: 16.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 8.0),
                    Text('Farmer has to input his pin',
                        style:
                            TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
                    SizedBox(height: 8.0),
                    Text('Please let the farmer enter his 4-digit code.',
                        style: TextStyle(fontSize: 13.0, color: AppColors.grey)),
                    SizedBox(height: 40.0),
                    Center(
                      child: Container(
                        width: 250,
                        child: PinCodeFields(
                          length: 4,
                          controller: newTextEditingController,
                          focusNode: focusNode,

                          textStyle: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold),
                          keyboardType: TextInputType.number,
                          borderWidth: 1.5,
                          activeBorderColor: AppColors.primaryColor,
                          borderColor: AppColors.grey,
                          obscureText: true,

                          // obscureCharacter: '❌',
                          // animationDuration: const Duration(milliseconds: 200),
                          // animationCurve: Curves.easeInOut,
                          // switchInAnimationCurve: Curves.easeIn,
                          // switchOutAnimationCurve: Curves.easeOut,
                          // animation: Animations.SlideInDown,
                          onComplete: (result) {
                            // Your logic with code
                            print(result);
                            inputPin = result;

                            // showDialog(
                            //     context: context,
                            //     builder: (context) {
                            //       return AlertDialog(
                            //         title: Text("Pin"),
                            //         content: Text('Pin entered is $result'),
                            //       );
                            //     });
                          },
                        ),
                      ),
                    ),
                    SizedBox(height: 32.0),
                    isPinInvalid
                        ? Container(
                            width: double.infinity,
                            child: Text(
                                'The pin farmer entered is incorrect.\nCheck and try again.',
                                style: TextStyle(color: AppColors.red),
                                textAlign: TextAlign.center))
                        : SizedBox(height: 30.0),
                    SizedBox(height: 50.0),
                  ],
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: CustomButton(
                    title: 'Checkout Bundles',
                    isFilled: true,
                    onTap: () {
                      print(widget.farmer.pin.toString());
                      if (inputPin == widget.farmer.pin.toString()) {
                        setState(() => _isLoading = true);
                        _apiService
                            .createLoan(widget.farmer, widget.loanBundlesIds)
                            .then((value) {
                              setState(() => _isLoading = false);
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => FarmerSucessScreen(),
                          ));
                        });
                      } else {
                        setState(() {
                          isPinInvalid = true;
                        });
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        _isLoading ? Loading() : Container(),
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
