import 'package:fc_inputs/api/api_service.dart';
import 'package:fc_inputs/models/user.dart';
import 'package:fc_inputs/screens/change_password_screen.dart';
import 'package:fc_inputs/screens/edit_details_screen.dart';
import 'package:fc_inputs/screens/signin_screen.dart';
import 'package:fc_inputs/screens/welcome_screen.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  ApiService _apiService = ApiService();

  User user;

  _getUserDetails() async {
    user = User();
    user.firstName = prefs.getString('FC_INPUTS_FIRST_NAME');
    user.lastName = prefs.getString('FC_INPUTS_LAST_NAME');
    user.phoneNumber = prefs.getString('FC_INPUTS_PHONE');
    user.farmStateId = prefs.getString('FC_INPUTS_STATE_ID');
    user.farmStateName = prefs.getString('FC_INPUTS_STATE_NAME');
    user.roleId = prefs.getString('FC_INPUTS_ROLE_ID');
    // print(user.farmStateId);
  }

  @override
  void initState() {
    _getUserDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 32.0, horizontal: 32.0),
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              child: Text('Make changes to your profile \nright here!!!',
                  style: TextStyle(color: AppColors.grey),
                  textAlign: TextAlign.center),
            ),
            SizedBox(height: 8.0),
            Container(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              width: double.infinity,
              height: 160.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      Image(
                        image: AssetImage(
                            'assets/images/profile-picture-placeholder.png'),
                        width: 120.0,
                      ),
                      Image(
                        image: AssetImage('assets/images/circle-img.png'),
                        width: 100.0,
                      ),
                      // PNetworkImage("https://picsum.photos/200"),
                    ],
                  ),
                  SizedBox(width: 16.0),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/images/add.png'),
                        width: 40.0,
                      ),
                      SizedBox(height: 8.0),
                      Image(
                        image: AssetImage('assets/images/delete.png'),
                        width: 40.0,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 24.0),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    topRight: Radius.circular(10.0)),
                border: Border.all(
                  width: 0.0,
                  color: AppColors.grey,
                ),
              ),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          EditDetailsScreen(user: user),
                    ),
                  );
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Image(
                          image: AssetImage('assets/images/edit.png'),
                          width: 20.0,
                        ),
                        SizedBox(width: 16.0),
                        Text('Edit Details'),
                      ],
                    ),
                    Icon(Icons.chevron_right,
                        color: AppColors.primaryColor, size: 32.0),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0)),
                border: Border.all(
                  width: 0.0,
                  color: AppColors.grey,
                ),
              ),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => ChangePasswordScreen(),
                    ),
                  );
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Image(
                          image: AssetImage('assets/images/password.png'),
                          width: 20.0,
                        ),
                        SizedBox(width: 16.0),
                        Text('Change Password'),
                      ],
                    ),
                    Icon(Icons.chevron_right,
                        color: AppColors.primaryColor, size: 32.0),
                  ],
                ),
              ),
            ),
            SizedBox(height: 64.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.power_settings_new, color: AppColors.redLogout),
                SizedBox(width: 16.0),
                InkWell(
                  onTap: () => Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (BuildContext context) => WelcomeScreen(),
                    ),
                  ),
                  child: Text('Logout',
                      style: TextStyle(
                          color: AppColors.redLogout, fontSize: 16.0)),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
