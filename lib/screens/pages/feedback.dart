import 'package:fc_inputs/api/api_service.dart';
import 'package:fc_inputs/screens/home_screen.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/custom_button.dart';
import 'package:fc_inputs/widgets/loading.dart';
import 'package:fc_inputs/widgets/mytext_formfield.dart';
import 'package:fc_inputs/widgets/toast_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

class FeedBack extends StatefulWidget {
  @override
  _FeedBackState createState() => _FeedBackState();
}

class _FeedBackState extends State<FeedBack> {
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  ApiService _apiService = ApiService();
  bool _isLoading = false;

  TextEditingController _controllerPhone = TextEditingController();
  TextEditingController _controllerFeedback = TextEditingController();

  @override
  void dispose() {
    _controllerPhone.dispose();
    _controllerFeedback.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      appBar: AppBar(
        title: Text('Feedback'),
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 12.0),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    child: Text(
                        'A farmer has a feedback? \nSend message to the farmcrowdy team \nthrough this medium.',
                        style: TextStyle(color: AppColors.grey),
                        textAlign: TextAlign.center),
                  ),
                  SizedBox(height: 16.0),
                  MyTextFormField(
                    controller: _controllerPhone,
                    hintText: 'Farmer\'s Phone number',
                    helperText: 'Length of phone number',
                    maxLength: 11,
                    showCounter: true,
                    textInputType: TextInputType.number,
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Enter phone number';
                      }
                      return null;
                    },
                  ),
                  MyTextFormField(
                    controller: _controllerFeedback,
                    hintText: 'Feedback',
                    maxLines: 7,
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Enter password';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 32.0),
                  CustomButton(
                    title: 'Send Feedback',
                    isFilled: true,
                    onTap: () {
                      String phone = _controllerPhone.text.toString().trim();
                      String feedback =
                          _controllerFeedback.text.toString().trim();

                      if (phone.isEmpty || feedback.isEmpty) {
                        _scaffoldState.currentState.showSnackBar(SnackBar(
                          content: Text("Enter phone number and feedback"),
                        ));
                      } else if (phone.length != 11) {
                        _scaffoldState.currentState.showSnackBar(SnackBar(
                          content: Text("Enter a valid phone number"),
                        ));
                      } else {
                        setState(() => _isLoading = true);
                        _apiService
                            .createFeedback(phone, feedback)
                            .then((value) {
                          setState(() => _isLoading = false);
                          if (value is bool) {
                            // Navigator.pop(_scaffoldState.currentState.context);
                            Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                builder: (BuildContext context) => HomeScreen(),
                              ),
                            );
                            // showToast("Feebback successfully submitted");

                            showToastWidget(ToastText(text:'Feedback submitted successfully'));
                          } else {
                            _scaffoldState.currentState.showSnackBar(SnackBar(
                              content: Text(value?.toString() ??
                                  "Unable to submit feedback. Try again!"),
                            ));
                          }
                        });
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
          _isLoading ? Loading() : Container(),
        ],
      ),
    );
  }
}


