import 'package:fc_inputs/api/api_service.dart';
import 'package:fc_inputs/models/user.dart';
import 'package:fc_inputs/screens/confirm_farmer_screen.dart';
import 'package:fc_inputs/screens/farmers_disbursed_screen.dart';
import 'package:fc_inputs/screens/farmers_disbursed_today_screen.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shimmer/shimmer.dart';
import 'package:intl/intl.dart';

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  ApiService _apiService = ApiService();
  final currency = new NumberFormat.currency(symbol: "\₦ ");

  // SharedPreferences prefs;
  User user;

  List<dynamic> metric = [null, null];

  _getUserDetails() async {
    user = User();
    user.firstName = prefs.getString('FC_INPUTS_FIRST_NAME');
    user.lastName = prefs.getString('FC_INPUTS_LAST_NAME');
    user.phoneNumber = prefs.getString('FC_INPUTS_PHONE');
    user.farmStateId = prefs.getString('FC_INPUTS_STATE');
    user.roleId = prefs.getString('FC_INPUTS_ROLE_ID');

    metric = await _apiService.getDisbursedMetric();
    setState(() {});
  }

  String greeting() {
    var hour = DateTime.now().hour;
    if (hour < 12) {
      return 'morning';
    }
    if (hour < 17) {
      return 'afternoon';
    }
    return 'evening';
  }

  @override
  void initState() {
    _getUserDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: AppBar(
        title: Text(
          'Home',
          style: GoogleFonts.inter(
              textStyle: TextStyle(color: AppColors.secondaryColor)),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
        // actions: [
        //   IconButton(
        //     icon: Icon(Icons.notifications),
        //     onPressed: () {
        //       Navigator.of(context).push(
        //         MaterialPageRoute(
        //           builder: (BuildContext context) => NotificationScreen(),
        //         ),
        //       );
        //     },
        //   ),
        //   SizedBox(width: 8.0),
        // ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 16.0),
              Text(
                "Good ${greeting()} ${user.firstName},",
                // 'Good day, ${user.firstName}',
                style: TextStyle(fontSize: 13.0, color: AppColors.grey),
              ),
              SizedBox(height: 8.0),
              Text(
                'Want to get a Farmer an input?',
                style: TextStyle(
                    fontSize: 16.0,
                    color: AppColors.secondaryColor,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 16.0),
              CustomButton(
                title: 'Disburse Input',
                isFilled: true,
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => ConfirmFarmerScreen(),
                    ),
                  );
                },
              ),
              // CustomButton(
              //   title: 'Select Insurance Claims for Farmers',
              //   isFilled: false,
              //   onTap: () {},
              // ),
              SizedBox(height: 32.0),
              Text(
                'Dashboard Summary',
                style: TextStyle(fontSize: 16.0, color: AppColors.grey),
              ),
              SizedBox(height: 10.0),
              // DasboardItem(
              //     title: 'Metric Tonnes Disbursed in Total',
              //     value: '273,273,000,000'),
              // DasboardItem(
              //     title: 'Metric Tonnes Disbursed Today',
              //     value: '273,273,000,000'),

              DasboardItem(
                title: 'Farmers Disbursed - Today',
                value: metric[1] != null ? currency.format(metric[1]): null,
                buttonText: 'View All Farmers',
                hasButton: true,
                buttonOnTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          FarmersDisbursedTodayScreen(),
                    ),
                  );
                },
              ),
              DasboardItem(
                title: 'Total Farmers Disbursed',
                value: metric[0] != null ? currency.format(metric[1]): null,

                // value: currency.format(metric[0]),

                buttonText: 'View All Farmers',
                hasButton: true,
                buttonOnTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          FarmersDisbursedScreen(),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class DasboardItem extends StatelessWidget {
  final String title;
  final dynamic value;
  final String buttonText;
  final bool hasButton;
  final Function buttonOnTap;

  const DasboardItem({
    @required this.title,
    @required this.value,
    this.buttonText,
    this.hasButton = false,
    this.buttonOnTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10.0),
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 24.0),
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        border: Border.all(
          width: 0.0,
          color: AppColors.grey,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontSize: 12.0, color: AppColors.grey),
          ),
          SizedBox(height: 12.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              value != null
                  ? Text(
                      value.toString(),
                      style: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.bold),
                    )
                  : Shimmer.fromColors(
                      baseColor: AppColors.black,
                      highlightColor: AppColors.primaryColor,
                      child: Text(
                        'Loading',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
              hasButton
                  ? InkWell(
                      onTap: buttonOnTap,
                      child: Container(
                        // height: 56.0,
                        padding: EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 10.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14.0),
                          color: AppColors.primaryColor,
                        ),
                        child: Center(
                          child: Text(buttonText,
                              style: TextStyle(
                                  fontSize: 10.0,
                                  color: AppColors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                      ),
                    )
                  : Container(),
            ],
          ),
        ],
      ),
    );
  }
}
