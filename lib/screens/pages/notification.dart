import 'package:fc_inputs/screens/message_feedback_screen.dart';
import 'package:fc_inputs/screens/message_input_screen.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/chat_item.dart';
import 'package:fc_inputs/widgets/notification_item.dart';
import 'package:flutter/material.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationState createState() => _NotificationState();
}

class _NotificationState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notifications'),
        centerTitle: true,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
        child: ListView.separated(
          itemCount: 3,
          itemBuilder: (context, index) {
            return NotificationItem(
              name: "System Maintenance",
              time: '2:30 pm',
              msg: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, consectetur adipiscing elit.',
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) =>
                        MessageFeedbackScreen(),
                  ),
                );
              },
            );
          },
          separatorBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Align(
                alignment: Alignment.center,
                child: Container(
                  height: 0.5,
                  width: MediaQuery.of(context).size.width,
                  child: Divider(),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
