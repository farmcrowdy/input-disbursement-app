import 'package:fc_inputs/screens/message_feedback_screen.dart';
import 'package:fc_inputs/screens/message_input_screen.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/chat_item.dart';
import 'package:flutter/material.dart';

class Message extends StatefulWidget {
  @override
  _MessageState createState() => _MessageState();
}

class _MessageState extends State<Message> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Message'),
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
        child: Column(
          children: <Widget>[
            ChatItem(
              dp: 'assets/images/circle-img.png',
              name: "FC Input Team",
              time: '21:34',
              msg:
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet.',
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => MessageInputScreen(),
                  ),
                );
              },
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                height: 0.5,
                child: Divider(),
              ),
            ),
            ChatItem(
              dp: 'assets/images/circle-img.png',
              name: "FC Input Feedback Team",
              time: '21:34',
              msg:
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, consectetur adipiscing elit.',
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => MessageFeedbackScreen(),
                  ),
                );
              },
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                height: 0.5,
                width: MediaQuery.of(context).size.width,
                child: Divider(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
