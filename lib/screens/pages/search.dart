import 'package:fc_inputs/api/api_service.dart';
import 'package:fc_inputs/models/farmer.dart';
import 'package:fc_inputs/screens/search_result_screen.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/custom_button.dart';
import 'package:fc_inputs/widgets/loading.dart';
import 'package:fc_inputs/widgets/mytext_formfield.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Search extends StatefulWidget {
  Search({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  ApiService _apiService = ApiService();
  bool _isLoading = false;

  TextEditingController _controllerPhone = TextEditingController();

  GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Search'),
        centerTitle: true,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
        //  actions: [
        //   IconButton(
        //     icon: Icon(Icons.notifications),
        //     onPressed: () {
        //       Navigator.of(context).push(
        //         MaterialPageRoute(
        //           builder: (BuildContext context) => NotificationScreen(),
        //         ),
        //       );
        //     },
        //   ),
        //   SizedBox(width: 8.0),
        // ],
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 16.0),
                  Center(
                    child: Text('Search for details \nconcerning a farmer',
                        style: TextStyle(color: AppColors.grey),
                        textAlign: TextAlign.center),
                  ),
                  SizedBox(height: 16.0),
                  MyTextFormField(
                    controller: _controllerPhone,
                    hintText: 'Farmer\'s Phone number',
                    helperText: 'Length of phone number',
                    textInputType: TextInputType.number,
                    suffixIcon: Icon(Icons.search),
                    showCounter: true,
                    maxLength: 11,
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Enter phone number';
                      } 
                      return null;
                    },
                  ),
                  SizedBox(height: 16.0),
                  CustomButton(
                    title: 'Search for Farmer',
                    isFilled: true,
                    onTap: () {
                      String phone = _controllerPhone.text.toString().trim();

                      if (phone.isEmpty) {
                        _scaffoldState.currentState.showSnackBar(SnackBar(
                          content:
                              Text("Enter Farmer's Phone number to proceed"),
                        ));
                      }  else if (phone.length != 11) {
                        _scaffoldState.currentState.showSnackBar(SnackBar(
                          content: Text("Enter a valid phone number"),
                        ));
                      } else {
                        setState(() => _isLoading = true);
                        _apiService.getFarmerByPhone(phone).then((value) async {
                          setState(() => _isLoading = false);

                          if (value is Farmer) {
                            String state =
                                await _apiService.getStateById(value.state);
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    SearchResultScreen(
                                        farmer: value, state: state),
                              ),
                            );
                          } else {
                            _scaffoldState.currentState.showSnackBar(SnackBar(
                              content: Text(
                                  value?.toString() ?? "Unable to find farmer"),
                            ));
                          }
                        });
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
          _isLoading ? Loading() : Container(),
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
