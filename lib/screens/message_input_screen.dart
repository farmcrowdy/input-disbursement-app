import 'dart:math';

import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/chat_bubble.dart';
import 'package:fc_inputs/widgets/chat_item.dart';
import 'package:flutter/material.dart';

class MessageInputScreen extends StatefulWidget {
  @override
  _MessageInputScreenState createState() => _MessageInputScreenState();
}

class _MessageInputScreenState extends State<MessageInputScreen> {
  static Random random = Random();
  static List messages = [
    "Hey, how are you doing?",
    "Are you available tomorrow?",
    "Do you have milk available",
    "I'm currently in Lagos",
    "mdairy is a very great app",
    "What are the your current grazing patterns",
    "mdairy app is awesome",
    "Can you help make a transaction",
    "I will deliver the milk tomorrow",
    "I just delivered the milk",
    "The data log has been added to mdairy",
  ];
  List conversation = List.generate(
      10,
      (index) => {
            "username": "Group ${random.nextInt(20)}",
            "time": "${random.nextInt(50)} min ago",
            "type": "text",
            "replyText": messages[random.nextInt(10)],
            "isMe": random.nextBool(),
            "isGroup": false,
            "isReply": random.nextBool(),
          });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FC Input Team'),
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: 32.0),
              child: ListView.builder(
                padding: EdgeInsets.all(0),
                itemCount: conversation.length,
                reverse: true,
                itemBuilder: (BuildContext context, int index) {
                  Map msg = conversation[index];
                  return ChatBubble(
                    dp: 'assets/images/circle-img.png',
                    message:
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    username: 'Username',
                    time: '2:30pm',
                    type: 'text',
                    replyText: 'asdasd',
                    isMe: random.nextBool(),
                    isGroup: msg['isGroup'],
                    // isReply: random.nextBool(),
                    isReply: false,
                    replyName: "name",
                  );
                },
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[500],
                    offset: Offset(0.0, 1.5),
                    blurRadius: 4.0,
                  ),
                ],
              ),
              constraints: BoxConstraints(
                maxHeight: 190,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 32.0, vertical: 16.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        style: TextStyle(
                          fontSize: 15.0,
                          color: Theme.of(context).textTheme.title.color,
                        ),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10.0),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0),
                            borderSide: BorderSide(
                              color: AppColors.grey,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Theme.of(context).primaryColor,
                            ),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          hintText: "Type your message here...",
                          hintStyle: TextStyle(
                            fontSize: 15.0,
                            color: Theme.of(context).textTheme.title.color,
                          ),
                        ),
                        maxLines: null,
                      ),
                    ),
                    SizedBox(width: 10.0),
                    Container(
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: AppColors.primaryColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Image(
                          image: AssetImage('assets/images/send.png'),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
