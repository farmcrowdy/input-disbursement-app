import 'package:fc_inputs/api/api_service.dart';
import 'package:fc_inputs/screens/home_screen.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/custom_button.dart';
import 'package:fc_inputs/widgets/loading.dart';
import 'package:fc_inputs/widgets/mytext_formfield.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SignInScreen extends StatefulWidget {
  SignInScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  ApiService _apiService = ApiService();
  bool _isLoading = false;

  TextEditingController _controllerPhone = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();

  @override
  void dispose() {
    _controllerPhone.dispose();
    _controllerPassword.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      appBar: AppBar(
        title: Text(
          'Welcome Back',
          style: GoogleFonts.inter(
              textStyle: TextStyle(color: AppColors.secondaryColor)),
        ),
        centerTitle: true,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 20.0),
              child: Form(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 12.0),
                    Center(
                      child: Text(
                          'Welcome back. Log In to add farmers \nto bundles among other things.'),
                    ),
                    SizedBox(height: 16.0),
                    MyTextFormField(
                      controller: _controllerPhone,
                      hintText: 'Phone number',
                      maxLength: 11,
                      textInputType: TextInputType.number,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter phone number';
                        }
                        return null;
                      },
                    ),
                    MyTextFormField(
                      controller: _controllerPassword,
                      hintText: 'Password',
                      isPassword: true,
                      // textInputType: TextInputType.visiblePassword,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter password';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 8.0),
                    Text(
                      'Forgot Password?',
                      style: TextStyle(
                        color: AppColors.grey,
                        fontSize: 12.0,
                      ),
                    ),
                    SizedBox(height: 16.0),
                    CustomButton(
                      title: 'Log In',
                      isFilled: true,
                      onTap: () {
                        String phone = _controllerPhone.text.toString().trim();
                        String password =
                            _controllerPassword.text.toString().trim();

                        if (phone.isEmpty || password.isEmpty) {
                          _scaffoldState.currentState.showSnackBar(SnackBar(
                            content: Text(
                                "Enter phone number and password to log in"),
                          ));
                        } else {
                          setState(() => _isLoading = true);
                          _apiService.login(phone, password).then((isSuccess) {
                            setState(() => _isLoading = false);
                            if (isSuccess) {
                              // Navigator.pop(_scaffoldState.currentState.context);
                              Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                  builder: (BuildContext context) => HomeScreen(),
                                ),
                              );
                            } else {
                              _scaffoldState.currentState.showSnackBar(SnackBar(
                                content: Text("Invalid login credentials."),
                              ));
                            }
                          });
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          _isLoading ? Loading() : Container(),
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
