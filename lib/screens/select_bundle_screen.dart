import 'package:fc_inputs/models/crop_type.dart';
import 'package:fc_inputs/models/farmer.dart';
import 'package:fc_inputs/models/load_bundle.dart';
import 'package:fc_inputs/screens/farmer_pin_screen.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class SelectBundleScreen extends StatefulWidget {
  SelectBundleScreen({Key key, this.title, this.farmer, this.cropType})
      : super(key: key);
  final Farmer farmer;
  final CropType cropType;
  final String title;

  @override
  _SelectBundleScreenState createState() => _SelectBundleScreenState();
}

class _SelectBundleScreenState extends State<SelectBundleScreen> {
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  final currency = new NumberFormat.currency(symbol: "\₦ ");

  List<bool> values = [];
  List<int> selectedLoanBundleIds = [];

  double totalAmount = 0;

  String capitalize(String string) {
    if (string == null) {
      throw ArgumentError("string: $string");
    }

    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }

  @override
  Widget build(BuildContext context) {
    var textStyle2 = TextStyle(color: AppColors.grey, height: 1.5);
    return Scaffold(
      key: _scaffoldState,
      backgroundColor: AppColors.white,
      appBar: AppBar(
        title: Text(
          'Select Bundle',
          style: GoogleFonts.inter(
              textStyle: TextStyle(color: AppColors.secondaryColor)),
        ),
        centerTitle: true,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 32.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 20.0),
            Text('Step 3 of 5', style: TextStyle(color: AppColors.grey)),
            SizedBox(height: 16.0),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10.0),
                        topRight: Radius.circular(10.0)),
                    border: Border.all(
                      width: 0.0,
                      color: AppColors.grey,
                    )),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 8.0),
                    Text(capitalize(widget.cropType.name),
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold)),
                    SizedBox(height: 8.0),
                    Text('${widget.cropType.size} loan bundles available',
                        style:
                            TextStyle(fontSize: 13.0, color: AppColors.grey)),
                    SizedBox(height: 8.0),
                    Expanded(
                      child: Builder(
                        builder: (BuildContext context) {
                          List<LoanBundle> loanBundles =
                              widget.cropType.loanBundle;

                          // Instantiate an empty list with false
                          if (loanBundles.length != values.length) {
                            for (var i = 0; i < loanBundles.length; i++) {
                              values.add(false);
                              selectedLoanBundleIds.add(-1);
                            }
                          }
                          print(values.toString());
                          print(selectedLoanBundleIds.toString());

                          return ListView.separated(
                            shrinkWrap: true,
                            itemCount: loanBundles.length,
                            // itemCount: widget.cropType.size,
                            itemBuilder: (BuildContext context, int index) {
                              LoanBundle loanBundle = loanBundles[index];

                              return ListTileTheme(
                                contentPadding: EdgeInsets.all(0),
                                child: CheckboxListTile(
                                  activeColor: AppColors.primaryColor,
                                  controlAffinity:
                                      ListTileControlAffinity.leading,
                                  title: Text(''),
                                  subtitle: buildBundle(loanBundle),
                                  value: values[index],
                                  onChanged: (bool value) {
                                    setState(() {
                                      values[index] = value;

                                      if (value) {
                                        totalAmount += loanBundle.price;
                                        selectedLoanBundleIds[index] =
                                            loanBundle.id;
                                      } else {
                                        totalAmount -= loanBundle.price;
                                        selectedLoanBundleIds[index] = -1;
                                      }
                                    });
                                  },
                                ),
                              );
                            },
                            separatorBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.only(top: 16.0),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    height: 0.5,
                                    width:
                                        MediaQuery.of(context).size.width / 1.3,
                                    child: Divider(),
                                  ),
                                ),
                              );
                            },
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                    ),
                    border: Border.all(
                      width: 0.0,
                      color: AppColors.grey,
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Total',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        currency.format(totalAmount),
                        // '\₦ ${totalAmount.toString()}',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            CustomButton(
              title: 'Proceed',
              isFilled: true,
              onTap: () {
                if (totalAmount == 0) {
                  _scaffoldState.currentState.showSnackBar(SnackBar(
                    content:
                        Text("No bundle selected. Select bundle(s) to proceed"),
                  ));
                } else {
                  // Get the id of the selected bundles
                  List<int> res = [];
                  for (var i = 0; i < selectedLoanBundleIds.length; i++) {
                    if (selectedLoanBundleIds[i] != -1) {
                      res.add(selectedLoanBundleIds[i]);
                    }
                  }

                  print(res.toString());

                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => FarmerPinScreen(
                          farmer: widget.farmer, loanBundlesIds: res),
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  buildBundle(LoanBundle loanBundle) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
                          child: Text(capitalize(loanBundle.name),
                  style: TextStyle(
                      fontSize: 13.0,
                      color: AppColors.secondaryColor,
                      fontWeight: FontWeight.bold)),
            ),
            Text(
              // '\₦ ${loanBundle.price}',
              currency.format(loanBundle.price),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
        SizedBox(height: 8.0),
        Text(loanBundle.description,
            style: TextStyle(
                fontSize: 12.0, color: AppColors.grey, height: 1.5)),
        // Text('Tractor',
        //     style: TextStyle(
        //         fontSize: 12.0, color: AppColors.grey, height: 1.5)),
        // Text('Combiners',
        //     style: TextStyle(
        //         fontSize: 12.0, color: AppColors.grey, height: 1.5)),
        // Text('Harvester',
        //     style: TextStyle(
        //         fontSize: 12.0, color: AppColors.grey, height: 1.5)),
      ],
    );
  }
}
