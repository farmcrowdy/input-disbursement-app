import 'package:fc_inputs/screens/signin_screen.dart';
import 'package:fc_inputs/screens/signup_screen.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/custom_button.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatefulWidget {
  WelcomeScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/agric-bg.png'),
            fit: BoxFit.fill,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 32.0),
                      Text(
                        'Welcome',
                        style: TextStyle(
                            color: AppColors.secondaryColor,
                            fontSize: 30,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 10.0),
                      Row(
                        children: <Widget>[
                          Text(
                            'to ',
                            style: TextStyle(
                                color: AppColors.secondaryColor,
                                fontSize: 30,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            'Farmcrowdy',
                            style: TextStyle(
                                color: AppColors.primaryColor,
                                fontSize: 30,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      SizedBox(height: 10.0),
                      Text(
                        'Loans.',
                        style: TextStyle(
                            color: AppColors.primaryColor,
                            fontSize: 30,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 32.0),
                      Text(
                        'Farmers favourite loans tool \nproviding support with the growth of \ntheir agricultural produces and \nagricutural machiners.',
                        style: TextStyle(
                          color: AppColors.grey,
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                          height: 1.7,
                        ),
                      ),
                    ],
                  ),
                ),
                Column(
                  children: <Widget>[
                    CustomButton(
                      title: 'Sign Up',
                      isFilled: true,
                      onTap: () => Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => SignUpScreen(),
                        ),
                      ),
                    ),
                    CustomButton(title: 'Log In', isFilled: false, onTap: () => Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => SignInScreen(),
                        ),
                      ),),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
