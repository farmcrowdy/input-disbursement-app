import 'package:flutter/material.dart';
import 'package:fc_inputs/api/api_service.dart';
import 'package:fc_inputs/models/user.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/custom_button.dart';
import 'package:fc_inputs/widgets/mytext_formfield.dart';
import 'package:google_fonts/google_fonts.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class ChangePasswordScreen extends StatefulWidget {
  ChangePasswordScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  ChangePasswordScreenState createState() => ChangePasswordScreenState();
}

class ChangePasswordScreenState extends State<ChangePasswordScreen> {
  User user = new User();

  bool _isLoading = false;
  ApiService _apiService = ApiService();
  final _formKey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,

      appBar: AppBar(
        title: Text(
          'Change Password',
          style: GoogleFonts.inter(
              textStyle: TextStyle(color: AppColors.secondaryColor)),
        ),
        centerTitle: true,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 20.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 32.0, vertical: 8.0),
                      child: Text(
                          'Enter current password and new password to change your password',
                          style: TextStyle(color: AppColors.grey, height: 1.5),
                          textAlign: TextAlign.center),
                    ),
                    MyTextFormField(
                      hintText: 'Current Password',
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter current password';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        user.phoneNumber = value;
                      },
                    ),
                    MyTextFormField(
                      hintText: 'New Password',
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter new password';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        user.password = value;
                      },
                    ), MyTextFormField(
                      hintText: 'Confirm New Password',
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Enter new password';
                        }
                        return null;
                      },
                      onSaved: (String value) {
                        user.password = value;
                      },
                    ),
                    SizedBox(height: 8.0),
                  ],
                ),
                CustomButton(
                  title: 'Save Details',
                  isFilled: true,
                  onTap: () async {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      print(user.toString());

                      // _apiService.registerAgent(farmer).then((isSuccess) {
                      //   setState(() => _isLoading = false);
                      //   if (isSuccess) {
                      //     Navigator.pop(_scaffoldState.currentState.context);
                      //   } else {
                      //     _scaffoldState.currentState.showSnackBar(SnackBar(
                      //       content: Text("Submit Record failed"),
                      //     ));
                      //   }
                      // });
                      // Navigator.of(context).pop();
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
