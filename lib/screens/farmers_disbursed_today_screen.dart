import 'package:fc_inputs/api/api_service.dart';
import 'package:fc_inputs/models/loan_disbursed.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:fc_inputs/widgets/loading.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class FarmersDisbursedTodayScreen extends StatefulWidget {
  FarmersDisbursedTodayScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  FarmersDisbursedTodayScreenState createState() =>
      FarmersDisbursedTodayScreenState();
}

class FarmersDisbursedTodayScreenState
    extends State<FarmersDisbursedTodayScreen> {
  final currency = new NumberFormat.currency(symbol: "\₦ ");

  ApiService _apiService = ApiService();
  bool _isLoading = false;

  GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      backgroundColor: AppColors.white,
      appBar: AppBar(
        title: Text(
          'Farmers Disbursed To',
          style: GoogleFonts.inter(
              textStyle: TextStyle(color: AppColors.secondaryColor)),
        ),
        centerTitle: true,
        backgroundColor: AppColors.white,
        iconTheme: IconThemeData(
          color: AppColors.secondaryColor, //change your color here
        ),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 32.0, vertical: 32.0),
            child: Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 16.0),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        border: Border.all(
                          width: 0.0,
                          color: AppColors.grey,
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 8.0),
                          Text('Today',
                              style: TextStyle(
                                  fontSize: 15.0,
                                  color: AppColors.secondaryColor,
                                  fontWeight: FontWeight.bold)),
                          SizedBox(height: 16.0),
                          FutureBuilder(
                            // future: _apiService.getAllLoans(isTodayOnly: true),
                            future: _apiService.getLoansDisbursedToday(),
                            builder: (BuildContext context,
                                AsyncSnapshot<List<LoanDisbursed>> snapshot) {
                              if (snapshot.hasError) {
                                return Center(
                                  child: Text(
                                      "Something wrong with message: ${snapshot.error.toString()}"),
                                );
                              } else if (snapshot.connectionState ==
                                  ConnectionState.done) {
                                List<LoanDisbursed> loans = snapshot.data;
                                return Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 4.0, horizontal: 8.0),
                                  child: ListView.separated(
                                    scrollDirection: Axis.vertical,
                                    shrinkWrap: true,
                                    physics: ClampingScrollPhysics(),
                                    reverse: true,
                                    itemCount: loans.length,
                                    itemBuilder: (context, index) {
                                      LoanDisbursed loan = loans[index];

                                      String _loanBundles = "";
                                      for (var item in loan.loanBundles) {
                                        _loanBundles = "$_loanBundles, $item"; 
                                      }

                                      return InkWell(
                                        child: Container(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 8.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text('${index + 1}.'),
                                              SizedBox(width: 16.0),
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                      "${loan.farmer.firstName} ${loan.farmer.lastName}"),
                                                  SizedBox(height: 8.0),
                                                  Text(
                                                      'Phone number: ${loan.farmer.phone}',
                                                      style: TextStyle(
                                                          color: AppColors.grey,
                                                          fontSize: 12.0)),
                                                  SizedBox(height: 4.0),
                                                  Text(
                                                      'Amount: ${currency.format(loan.amount)}',
                                                      style: TextStyle(
                                                          color: AppColors.grey,
                                                          fontSize: 12.0)),
                                                  SizedBox(height: 4.0),
                                                  Text(
                                                      // 'Selected Crop: ${loan.loanBundles}',
                                                      'Selected Crop: ${_loanBundles.substring(2)}',
                                                      style: TextStyle(
                                                          color: AppColors.grey,
                                                          fontSize: 12.0)),
                                                  SizedBox(height: 4.0),
                                                  
                                                  // Text(
                                                  //   'Selected Bundles: Farm Mechanization',
                                                  //   style: TextStyle(
                                                  //       color: AppColors.grey,
                                                  //       fontSize: 12.0),
                                                  // ),
                                                  // Container(
                                                  //   height: 400,
                                                  //   child: ListView.builder(
                                                  //       shrinkWrap: true,
                                                  //       // physics:NeverScrollableScrollPhysics(),
                                                  //       physics: ClampingScrollPhysics(),
                                                  //       // itemCount: loan.loanBundles.length,
                                                  //       itemCount: 2,
                                                  //       itemBuilder:
                                                  //           (context, index) {
                                                  //         return Text("asd");
                                                  //       }),
                                                  // )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        onTap: () {
                                          Navigator.of(context).pop();
                                        },
                                      );
                                    },
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return Padding(
                                        padding: const EdgeInsets.only(
                                            top: 10.0, bottom: 4.0),
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Container(
                                            height: 0.5,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                1.3,
                                            child: Divider(),
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                );
                              } else {
                                return Center(
                                    child: Padding(
                                  padding: const EdgeInsets.all(32.0),
                                  child: CircularProgressIndicator(),
                                ));
                              }
                            },
                          ),
                        
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          _isLoading ? Loading() : Container(),
        ],
      ),
    );
  }
}
