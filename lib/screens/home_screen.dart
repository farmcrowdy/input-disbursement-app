import 'package:fc_inputs/screens/pages/home.dart';
import 'package:fc_inputs/screens/pages/profile.dart';
import 'package:fc_inputs/screens/pages/feedback.dart';
import 'package:fc_inputs/screens/pages/search.dart';
import 'package:fc_inputs/utils/colors.dart';
import 'package:flutter/material.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // Properties & Variables needed

  int currentTab = 0; // to keep track of active tab index
  Widget currentScreen = Home(); // Our first view in viewport
  final List<Widget> screens = [
    Home(),
    Search(),
    FeedBack(),
    Profile(),
  ]; // to store nested tabs
  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              buildMaterialButton("Home", "home", 0),
              buildMaterialButton("Search", "search", 1),
              buildMaterialButton("Feedback", "feedback", 2),
              buildMaterialButton("Profile", "profile", 3),
            ],
          ),
        ),
      ),
    );
  }

  MaterialButton buildMaterialButton(
      String title, String icon, int position) {
    return MaterialButton(
      // minWidth: 40,
      onPressed: () {
        setState(() {
          currentScreen = screens[
              position]; // if user taps on this dashboard tab will be active
          currentTab = position;
        });
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/$icon.png'),
            width: 20.0,
            color: currentTab == position ? AppColors.primaryColor : Colors.grey,
          ),
          // Icon(
          //   icon,
          //   size: 20.0,
          //   color: currentTab == position ? AppColors.primaryColor : Colors.grey,
          // ),
          SizedBox(height: 5.0),
          Text(
            title,
            style: TextStyle(
              fontSize: 12.0,
              color:
                  currentTab == position ? AppColors.primaryColor : Colors.grey,
            ),
          ),
        ],
      ),
    );
  }
}
